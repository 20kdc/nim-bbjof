# bbjof

## Description

ByteByteJump-handling library in/for Nim, and associated tools.

## License

See COPYING.txt (SPDX: CC0-1.0)
