import unittest
import bbjof
import bbjof/verify
import bbjof/prims
import bbjof/write
import bbjof/merge
import bbjof/a4core
import bbjof/parseint
import streams
import md5
import tables

test "parseBBJOFInt is better at counting than a weird fluffy thing":
  assert parseBBJOFInt("0") == 0
  assert parseBBJOFInt("1") == 1
  assert parseBBJOFInt("2") == 2
  assert parseBBJOFInt("0xA") == 10
  assert parseBBJOFInt("0xF") == 15
  assert parseBBJOFInt("0x1F") == 31
  assert parseBBJOFInt("0x3F") == 63
  assert parseBBJOFInt("-0x3F") == -63
  assert parseBBJOFInt("-0xF0") == -240
  assert parseBBJOFInt("200") == 200
  assert parseBBJOFInt("-200") == -200

proc testrun(val: BBJOF, hash: string): void =
  val.verify()
  var stream = newStringStream()
  stream.writeBBJOF(val)
  let actualMD5 = $toMD5(stream.data)
  echo actualMD5 & " == " & hash
  assert actualMD5 == hash

test "can synthesize 'byte' correctly":
  testrun(newBBJOFPrimByte(), "0dd67c0b30b4be5445e350804948c2d3")

test "linker merges imports correctly":
  var test = BBJOF(
    imports: @[
      # This order is extremely specific to test for a rather annoying bug
      "A",
      "B",
      "A", # it deletes this...
      "C",
      "C", # then the indexes shift, so when trying to delete this...
      "D"  # it deletes this instead
    ],
    exports: @[
      BBJOFExp(name: "Export_A", target: BBJOFRef(index: -3)),
      BBJOFExp(name: "Export_B", target: BBJOFRef(index: -2)),
      BBJOFExp(name: "Export_C", target: BBJOFRef(index: -4)),
      BBJOFExp(name: "Export_D", target: BBJOFRef(index: -6))
    ]
  )
  test.link()
  assert len(test.imports) == 4
  assert test.imports[0] == "A"
  assert test.imports[1] == "B"
  assert test.imports[2] == "C"
  assert test.imports[3] == "D"
  assert test.exports[0].target.index == -1
  assert test.exports[1].target.index == -2
  assert test.exports[2].target.index == -3
  assert test.exports[3].target.index == -4

test "linker merges imports and exports correctly":
  var test = BBJOF(
    imports: @[
      "A"
    ],
    allocs: @[
      BBJOFAlloc(
        size: 1,
        alignStart: 0,
        alignStep: 1
      )
    ],
    exports: @[
      BBJOFExp(name: "A", target: BBJOFRef(index: 0)),
    ],
    relocs: @[
      BBJOFRel(
        source: BBJOFRef(index: -1),
        addrByte: 0,
        # target is obvious
      )
    ]
  )
  test.link()
  assert len(test.imports) == 0
  assert len(test.exports) == 1
  assert len(test.relocs) == 1

test "A4 Core actually does it's job: address":
  var test = BBJOF(
    imports: @[
      "SRC",
      "TGT"
    ],
    relocs: @[
      BBJOFRel(
        source: BBJOFRef(index: -1, offset: 0),
        target: BBJOFRef(index: -2, offset: 0),
        addrByte: 0
      ),
      BBJOFRel(
        source: BBJOFRef(index: -1, offset: 0),
        target: BBJOFRef(index: -2, offset: 1),
        addrByte: 1
      )
    ]
  )
  var a4c = newTable[string, A4CoreAdjustment]()
  a4c["SRC"] = A4CoreAdjustment(
    kind: a4caAddress,
    source: @[
      A4CoreSource(
        label: "0",
        offset: 0,
        extract: -1,
        attachments: @["IDX"]
      ),
      A4CoreSource(
        label: "PAGE",
        extract: -1
      )
    ]
  )
  test.applyA4Core(a4c)
  test.link()
  assert len(test.imports) == 3
  assert test.imports[0] == "TGT"
  assert test.imports[1] == "0"
  assert test.imports[2] == "PAGE"
  assert len(test.exports) == 1
  assert test.exports[0].name == "IDX"
  assert test.exports[0].target.importIdx() == 0
  assert len(test.relocs) == 2
  # should constrain more really, but this'd take ages
  testrun(test, "70f2570823323aa73b50f562a8a10834")
