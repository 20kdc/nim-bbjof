.TH "BBJOF" "5" "June 2020" "A4/B" "A4/B Manual"
.SH NAME
bbjof \- object format for ByteByteJump machines
.SH SYNOPSIS
.B requires 
"bbjof >= 0.1.0"

.SH DESCRIPTION
ByteByteJump machines, ByteByte/Jump machines, and BytePusher machines
have a specific set of requirements for an object format.

BytePusher programs require complete and total inlining to deal with the
65536-instruction-per-frame limit.

Any efficient production of a program for such machines heavily relies on
merging objects and renaming symbols at massive scales.

Relocations need to be able to work with specific address bytes as part of the
specifics of self-modification.

There needs to be a method of tracking debugging information in some way for
these machines.

And reparsing the same low-level building blocks repeatedly must be avoided.

The format must scale to any ByteByteJump machine it is reasonable to emulate,
which implies at least support for 5 address bytes (a terabyte of memory).
(BBJOF supports up to 7 address bytes for future-proofing.)

It must be possible to write tiny, fast single-use programs which generate them
to serve as the building blocks, and larger programs which orchestrate the
construction of useful programs.

As such, it is useful to be able to have a representation of any fixed set of
code for these machines that is essentially "already inlined", fast to read,
simple, able to merge new objects while performing a minimal amount of work, and
has a representation of whatever specific situation lead to the generation of
a specific part of the code.

(It's also useful to see the "SEE ALSO" section; the machines in question make
the reasoning rather clear.)

.SS Integers, the magic number, & size classes

To support smaller machines (1 to 3-byte) and larger machines (4 to 7-byte)
efficiently, BBJOF files are split into two size classes.

The size class is determined by the first 4 bytes of the file.

Files in the
"BBJO"
(42424A4F) class use 32-bit big-endian signed integers.

Files in the
"BBJX"
(42424A5A) class use 64-bit big-endian signed integers.

The type referred to as
.IR BBJOFInt
is whichever one of these integer types is currently in use.

If merging two object files, the resulting size class is the largest of them.

The file extension for BBJOF files is
".bjo".

.SS Symbols

A BBJOF object has import and export symbols, which occupy a common namespace.

These represent abstract numbers that may not be known yet.

In the interest of tooling speed, it is allowed for a file to have an import
symbol and an export symbol occupying the same name.

Such a symbol is in effect exported.

It is also allowed to have multiple import symbols for the same name.

It is not, however, allowed to have multiple export symbols for the same name.

It is allowed and expected that the symbol
"0" is always at address 0 - a symbol named
"0" must either not resolve yet or must resolve to address 0.

.SS Strings

Strings in BBJOF are always prefixed with a
.IR BBJOFInt " size - however many bytes are stated there follow."

Strings are UTF-8. They cannot contain bytes below 32, equal to 127, or 255.
(It is inadvisable, but not disallowed, to use codepoints above 127
in symbol names, as these have to be referred to in other code.)

These are referred to as
.IR BBJOFString
in future structures.

.SS Structure

The structure of a BBJOF file is described by the following
"C-like"
struct.
(It is important to note that this is not actually valid C to my knowledge.)

.PP
.in +4n
.EX
uint32_t magic; // implies size class
// -- main block sizes
BBJOFInt importCount;
BBJOFInt exportCount;
BBJOFInt debugCount;
BBJOFInt allocCount;
BBJOFInt relocCount;
// -- main blocks
BBJOFString imports[importCount];
BBJOFExp exports[exportCount];
BBJOFExp debugs[debugCount];
BBJOFAlloc allocs[allocCount];
BBJOFReloc relocs[relocCount];
.EE
.in
.PP

.SS References

A reference is a method of referring to an import or to an allocation.

It has the form:

.PP
.in +4n
.EX
// If negative, then negate and then decrement: this refers to an import index.
// Otherwise, it refers to an allocation index.
BBJOFInt source;
// The offset relative to the source.
// It's important to note that given a chain of imports/exports, all offsets are
// applied along the way.
BBJOFInt offset;
.EE
.in
.PP

These are referred to as
.IR BBJOFRef
in future structures.

.SS Exports

An export has the form:
.PP
.in +4n
.EX
BBJOFRef source;
BBJOFString name;
.EE
.in
.PP

This structure is used for both export symbols and debug symbols as they both
tag a particular location with a name, though they are extremely behaviourally
distinct.

Specifically, debug symbols must never have any direct effect on execution
behaviour; they can be used to set breakpoints, but that is a separate action
that utilizes a debug symbol.

These are referred to as
.IR BBJOFExp
in the file structures.

.SS Allocations

An allocation has the form:
.PP
.in +4n
.EX
// If negative, this is uninitialized. Actual size is abs(size).
// A zero size is treated as an initialized data area of size 0.
// This is 'quasi-fixed'; it isn't really fixed until allocation.
BBJOFInt size;
// The position to start alignment checks at.
// If size is zero, no restrictions apply (ignore following rules)
// If alignStep is non-zero, the following must apply:
//   (alignStart >= 0 && alignStart < alignStep)
// If alignStep is zero, then the total bounds of the allocation must be
//  within the available memory space of the machine at both allocation and
//  conversion to binary time.
BBJOFInt alignStart;
// The size of the alignment field. If 0, then the allocation is fixed.
// Must always be positive.
BBJOFInt alignStep;
// Actual initialized data (if any).
uint8_t data[max(size, 0)];
.EE
.in
.PP

These are referred to as
.IR BBJOFAlloc
in the file structures.

.SS Relocations

A relocation has the form:
.PP
.in +4n
.EX
// The source of the value. The machine address referenced is the integer from
//  which the address byte is extracted.
BBJOFRef source;
// The address byte. 0 is the lowest byte, and this goes up to the highest.
BBJOFInt addrByte;
// The target byte to write into.
// This must not point at an uninitialized allocation.
BBJOFRef target;
.EE
.in
.PP

When the source points at a fixed allocation (i.e. the source value is known),
and the target is pointing at an allocation (i.e. not an import),
a relocation may be linked.

Linking writes a single byte into an allocation, and is removed in the process.

Linking failure is
.I not
sufficient cause for an error, as this only indicates the symbol must be linked at
a later stage; unresolved relocations error during any operation where that relocation's
resolution is an absolute requirement to continue, and no earlier.

(In other words, unresolved relocations, like unresolved imports with references, error during symbol deletion.)

These are referred to as
.IR BBJOFRel
in the file structures.

.SH OPERATIONS

.SS Symbol renaming

Renaming symbols applies specifically to import and export symbols.

Symbol renaming is hierarchial with
"."
as the separator. Renaming
"@0"
to
"@1"
must also rename anything starting with
"@0."
unless otherwise requested.

Symbol renaming checks must be sure to check for more-exact matches over less-exact matches.
(Luckily, a sort-by-length will work just fine for this.)

Renaming a symbol to
""
deletes it and all sub-symbols.

It is allowed to rename import symbols over other symbols, or renaming export
symbols over import symbols. Export symbols mustn't be renamed over other export
symbols, though.

It is not allowed to delete an import symbol unless it can be linked during the
deletion so that it becomes unused. In this case, it is required that this is
performed. (If this rule was not in place, merging would require linking.)

.SS Debug symbols

Debug symbols do not use a namespace. They are essentially "attached notes".
They may use any characters whatsoever without any negative consequence.
However, it's recommended to prepend the location of the merge control command
followed by a comma (
"/mnt/terabyte/src/verybigtestprogram.a4:123,"
) to any debug symbol encountered when merging.

.SS Merging objects

The basic operation for merging two BBJOF files is:

1. Get the maximum of the size classes.

2. Merge allocations and imports; this requires no changes to them at all.

3. Merge exports and relocations, applying ID offsets to allocation/import
references on (and only on) exports and relocations from the second object.

Note that linking is not required, though safe to perform; it's performed during
 symbol deletion and during other build steps anyway.

It's useful to perform linking and optimization on intermediates to reduce the
complexity of the final link/optimize pass.

.SS Building a program

Building a BBJOF program is a 5-stage process.

1. Optionally, linking (to connect internal references for optimizing)

2. Optionally, removal of non-host exports (so optimization does something)

3. Optionally, optimizing (to clean up kitchen-sink page/bank libraries)

4. Allocating (to make all allocations fixed)

5. Linking (so that all relocations to newly-fixed allocations are resolved)

6. Conversion to the final image / injection into a running system.

.SS Linking an object

An object is considered
.I linked
when there are no imports for which corresponding exports already exist,
and all relocations targetting fixed allocations have been performed.

It is not allowed to have multiple export symbols for the same name, and such an
error must be detected at this time (it can be detected at other times, but may
not for speed reasons)

An object is linked by:

1. Taking all imports for which corresponding exports
exist and replacing the references to those imports with references to the
export targets (removing the imports in the process).

Due to the possibility of long import/export chains, this first step
will have to be repeated until nothing changes.
A minimum of 256 passes must be performed.

2. Taking all relocations with fixed allocation sources, performing, and
then removing them.

.SS Allocating an object

An object is considered
.I allocated
when all allocations have fixed addresses.

Allocating does not necessarily imply that the object is 'finished'.
It is possible to create an object that is almost completely allocated apart
from some missing data, for example for use in a interpreter+code layout,
where there is no logical reason to reallocate the interpreter on every rebuild.
(In this case, the additional data would be inserted and the object would
undergo the build process again in full.)

Allocating an existing object is how the final
"program object"
is mapped in memory ready for immediate conversion to a binary image or loading
into a machine.

It is recommended that the allocation procedure proceed as follows:

1. Allocations that are already fixed must be handled first.

2. Some algorithm must be chosen to decide the ordering of allocation placement.
This is the critical part and may in fact only be truly optimal via brute force.
A general-purpose high-speed algorithm is a sort, highest alignment to lowest.
This can be optimized further with bucketing.
It's important to note that the order of allocation, but not the method, should
ignore the initialized/uninitialized split for optimal memory space utilization.

Finally, it is important to note that the result of allocation does count as a
valid form for an emulator to accept so long as no unresolved addresses exist.

.SS Conversion to final binary / injection into a running system

The process of conversion to final binary involves creating the smallest file
that covers all initialized allocations, and writing the initialized allocation
content into that file.

This requires that the file is both allocated and linked, i.e. that all
allocations in the file are fixed and the file does not have any relocations.

The process of injection into a running system assumes that someone has somehow
created a storage mechanism capable of hosting BBJOF files within a ByteByteJump
system. In this case, the allocation and linking stages occur in the context of
the system's symbols (made reflectively available in some manner) or whichever
symbol namespace the system chooses to provide to the BBJOF file being injected.

.SS Additional optimizations for objects: Linking

The toolset you should have received this with performs these operations as part of linking.
These are generally useful to perform unless you have a good reason:

1. Duplicate imports are folded into single imports.

2. Unused imports (Note: a debug symbol DOES NOT count as a use. It used to. Had interesting effects on the original A4.)

3.
"0" is a symbol that is always present (representing the literal address 0) specifically as a relocation source.
For any other use, the normal handling applies, but with this covered there should be no other uses.

.SS Additional optimizations for objects: Dead Allocation Removal

Objects may also have entire dead allocations removed.

It is important to note that this procedure is not necessarily 100% stable,
and it should not be performed without direct user confirmation (a special command).

Before optimizing in this way, an object
.I must
be linked to avoid allocations not being properly connected.


It is possible that some machine in future or past requires some data at an
arbitrary user-controlled offset that is not otherwise referenced.

Any allocation that is fixed in position is a 'root' - it is implicitly part of
the machine's memory map.
An allocation can be referenced or unreferenced.
Any export from an allocation makes the allocation referenced.
Any relocation modifying a referenced allocation makes the relocation's source
a referenced allocation.
This applies recursively.

Unreferenced allocations can be removed (this must include their relocations).

.SH "SEE ALSO"
https://esolangs.org/wiki/ByteByteJump (author unknown)

https://esolangs.org/wiki/BytePusher (Javamannen)

.BR bbjof-conventions (7)

.BR bbjof (1)

.SH AUTHOR
20kdc

