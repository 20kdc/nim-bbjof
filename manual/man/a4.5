.TH "A4" "5" "June 2020" "A4/B" "A4/B Manual"
.SH NAME
a4 \- the A4 programming language

.SH DESCRIPTION
.B a4
(intended to be stylized A⁴) is a programming language aimed at ByteByteJump machines.

This is technically A4/B, but the original A4 (along with A3, A2, and A1) aren't in use anymore. A4/B is a rewrite.

A4 is built around the concept of creating
.BR bbjof (5)
object files through a mixture of combination and occasional relocation reversal for self-modification reasons.

.SH A4 - BASIC CONCEPTS - BEFORE WE BEGIN

It is critically important to understand how BBJOF object files are meant to be
used before attempting to understand A4.

A BBJOF object file is not meant to be linked a single time.
(Some specific BBJOF object files are meant to be linked only a single time, but these are exceptions to the rule.)

A BBJOF object file represents a compiled storage for macros made up of large
amounts of distinct allocations, relocations, imports and exports.

A BBJOF object file contains the full content of every object file merged into
it during the course of compilation.

It is meant to be linked, separately, every single time it is invoked.
It is meant to have it's symbols renamed to connect it to other code.
It is meant to have it's symbols stripped to keep it from conflicting with it's
other copies.
And so on.

The ultimate expression of this is the existence of the
"instruction" object. This is a single instruction.
It may be instanced thousands of times in many files,
and those files instanced hundreds of times, and those files instanced tens of
times, and those files instanced once or twice.

These are parallel copies of the
"instruction" object - they are not and cannot be merged into a single instance.

(It's for this reason that the usage of a relatively fast language like
Nim for the compiler itself, and the ability to use Makefiles for A4 projects is
absolutely and critically important to the production use of the A4 compiler.
Even the overhead of using repeated .delete to delete linked relocations was a
critical performance bug at 73737 relocations, and that was simply the example
project for compilation of a simple BytePusher application. Larger applications
can have millions of relocations and can have several-hundred-megabyte
intermediate object files, until the final link removes the relocations.)

.SH A4 - SYNTAX

A4 syntax is split between lexing and parsing.

Comments are C-style (// or /* */ as appropriate).

Splitting characters are whitespace, all 3 kinds of bracket/brace (parens, square, curly),
semicolon, comma, add, power, modulo/percent, colon, dollars, and equals.

There is a
"string"-like construct - it uses double-quotes but has no escapes.

It's important to note here that A4 tokens do not (at time of writing) have any form of type annotations.
The double-quotes are removed, but otherwise strings are exactly like any other token.
(This means it's possible to abuse strings to access A4 internal scratchpad symbols.
These always start with a space. Do not access them.)

There are also block strings, which are more interesting.

Block strings follow the form of XML tags (<block>text</block>), which allows them to theoretically contain any sequence of bytes as the
"tag name" can be changed as required to escape whatever contents exist.
Note that the XML resemblence is not literal - the end-tag sequence will immediately terminate the block string regardless of if the begin-tag sequence was used again.

For formal lexing rules, please see bbjof/a4/lex.nim.

Meanwhile, in regards to parsing:

An A4 file contains an A4 block.

An A4 block is a semicolon-delimited set of A4 elements which represents a set of calls to perform.

There are various A4 elements and ways to write them:

1. Labels -
"hello".
(These may be offset with, say,
"+123", but that is technically a call.)

2. Numeric literals -
"255",
"0xFF"...
(These are converted to the
"0" label with the value as an offset, as a dedicated type would complicate the code for no benefit.)

3. Calls (this is a very general category, including including object files,
invoking compiler directives, and the basic operators).

These have three forms -
"A(B, C...)" (ordinary call),
"B A C" (infix call), or some special cases,
"%A" (the 'immediate' unary operator), and
"A[B, C...]" (the 'complex address' operator).

4. An element may be wrapped in () for infix precedence reasons.

5. Other A4 blocks can be wrapped in {}.
The contents of these blocks are essentially A4 files by themselves.

The precedence of these is (from innermost to outermost): brackets
"()", calls
"A(B, C...)", complex
"A[B, C...]", arbitrary 'infix operators' calls, offsetting
"A + B", extraction
"A$0", immediate
"%A", attachment
"A^B", copy
"A=B", equ
"A:B".

Note that arbitrary 'infix operators' calls being the innermost, and having
equal precedence, means you will likely have to wrap the LHS and RHS in
brackets in a lot of situations.

This is just a convenience feature anyway, and it causes massive issues with
the parser otherwise.

Be aware that it is possible that complexes will be moved to a precedence
ideally just inside
"=".

.SH A4 - BASIC CONCEPTS - CORE

The core operation behind A4 is an expanded symbol-rename combined with merging
and call/return linking.

In particular, it allows three things to be applied specifically to symbols
imported by an instanced object:

Firstly, the obvious matter of renaming and offsetting.
Any parameter for which nothing "special" happens is renamed to the provided
label, and the offset is increased (if the object parameter is imported) or
decreased (if the object parameter is exported) based on the given offset.

Thus if
"test+5" is passed as a parameter an object exports, then it is assumed that
as the export exports
"test+5", therefore test should be 5 less than the resulting address.

This also implies that BBJOF structural renaming applies to these
"not special" cases, which allows structures to be easily passed around.

Secondly, to allow specifying their targets on a per-byte basis,
also referred to here as complex address sources. This has the form
"A[B, C...]" where A is the 'base', and B/C are the lower bytes of the address.

Note that the lower bytes are aligned to the lowest part of the address.
This makes complex address syntax more or less machine-independent.

And thirdly, to have the places that use these bytes exported.

This operation is referred to as attaching, or
"A^B" - where A is the original byte value and B is a label being attached to it.

There is also the matter of indirection - syntactic sugar that generates
instructions - but that isn't really core.
It's enough to know that
"%" inhibits indirection for now.

Assume an abstract world of 4-parameter instructions, where the first exports a label,
the second is the source address, the third is the target address, and the fourth is the jump address.

Now assume that page_inc is, as per convention, a page-aligned set of 256 bytes, where each byte is it's address + 1.

Thus, the following:

(top, page_inc[%0^a], a, top)

represents an infinitely looping counter, starting at 0.

The 'top' labels keep the loop infinite - it exports 'top' and imports 'top', so they link.
The useful part is that the instruction writes to 'a', which is inside the source address of the instruction.
As such, the instruction updates where it receives the next value for 'a' from, and thus the counter increases.

.SH A4 - BASIC CONCEPTS - INDIRECTION

Most labels being passed around have their target byte read/written,
unless they are jump labels.

In other words, a label is generally used for either the byte at it's location,
or as a jump target.

It is also convenient for variables to act like ordinary variables in most
circumstances.

It thus follows that the complex address source syntax should be consistent with this.

As such, there are two types for an address expression -
Adjustments and Sources.

They are better described by the expectations of their receivers.

An Adjustment indicates the receiver a label, and will read/write to it
directly or jump to it as it wishes.
Absolutely all call parameters to external objects are Adjustments.

A Source indicates the receiver wants a byte. Complex address sources take these
in their byte suffix.

Passing an Adjustment to a Source receiver will cause A4 to generate an
instruction reading the byte from the label and placing it at the location of
said byte. This is indirection.

So to be clear, the types expected by common expressions are as follows:

"A[B^C]": Adjustment, Source, Label.

"A[%B]": Adjustment, Adjustment.

"A[B]": Adjustment, Source.

"A = B": Adjustment, Adjustment.

.SH A4 - BASIC CONCEPTS - EXTRACTION AND IMMEDIATE OPERATORS

The purpose of
"$" (the extraction operator) is as a different way to convert an Adjustment to
a Source.

Rather than reading the byte at the given address, it instead acquires a byte
from the numeric value of the address and uses that byte as a constant literal.

It has a right-hand-side indicating the target byte of the input, from 0
(the lowest byte) upwards.

"%" (the immediate operator) is a syntactic-sugar prefix equivalent to the
"$0" suffix.

When the receiver is a Source,
they ensure that the given number is included as-is.

These operations act differently when the receiver is itself an Adjustment.

Obviously, a NOP would be inappropriate and wouldn't preserve the semantics.

So their operation is based on how complex the expression inside them is.

If it is a constant expression without any import or export shenanigans, then
a reference to an
"id." symbol will be produced.

Get relocations involved, however, and the situation is forced - A4 will make a
reference to a byte of
"page_dec" in order to generate the correct relocations.

If the expression is more complex than this, then it is not reasonable to
convert the expression to an identity-table access and an error occurs.

This other mode of operation allows for more obviously-reading code such as
"lives = %5;" (as opposed to, for example,
"lives = id+5;" or
"lives = (page_id$0 page_id$1 %5);" - these are from older toolsets, so their
syntax does not match modern A4/B.)

Effectively, the result is the same -
"%" and
"$" find the appropriate way to interpret their target as a literal value rather
than an address to a value.

.SH A4 - BASIC CONCEPTS - CONTROL FLOW

A4 control flow is based on a
"hanging return" label.

This label starts as
"@call" - this is the default entrypoint to the A4 block.

When something is included that has a default entrypoint (
"@call"), it is connected to the hanging return, and a new hanging return is
generated.

When something is included that has a default exitpoint (
"@return"), it is connected to the hanging return. (It is important that this
occurs after the hanging return generation for an entrypoint if present.)

At the end, the hanging return is renamed to
"@return" - this is the resulting default exitpoint of the A4 block.

The only statements in an A4 block with any effect on anything are direct
references to labels (which are connected to the current hanging return), 
and stuff that includes calls.

.SH A4 - BASIC CONCEPTS - CALLS

A call, to compiler built-in or otherwise, receives 3 pieces of information.

1. The thing being called.

2. If the call is the root of a statement.

3. The parameters to the call.

A call to a non-compiler-builtin that is not the root of a statement gets
a hidden first parameter. This hidden first parameter allows writing code like
"page_inc: page_inc();" when appropriate.

Call parameters are handled as described in the previous sections - they use
the names
"@0",
"@1" in the object being instanced.

.SH A4 - A SIMPLE EXAMPLE

.PP
.in +4n
.EX
// This is a program header for the test system (--address-bytes 3 / --big-endian)

programHeader: !alloc(6, 0, 0, 1);

!attachReloc(_start, 1, programHeader + 4);
!attachReloc(_start, 0, programHeader + 5);

catLoop;
0xFFFF = 0xFFFF;
goto(catLoop);
.EE
.in
.PP

.SH A4 - SCOPING

A4 uses relatively strict scoping.

In particular:

1. Upon completion of a block, an
"alias map" is applied. Any export which is not aliased (using, for example,
"!alias" ((target, source), where the target is the external name such as
"@0") or
"!global" (aliases the given symbol to itself)) is deleted.

If it would automatically delete an import, it instead allows it through.
Note, however, that if an alias-to-nothing is created, this doesn't happen.

2. During a call, it strips out all parameters which are not referenced.

"@call" and
"@return" are always considered referenced if present, but ordered parameters
are only considered referenced if the call actually refers to them,
and non-standard parameters cannot be referenced by an ordinary call.

This does error if it would delete an import.

3. The usage of a closure as a call parameter uses special scoping rules that
are specific to them. See that section for more information.

.SH A4 - CLOSURES

Closures are a relatively new (introduced in original A4, but upgraded heavily
in A4/B) concept to this series.

They're not quite like closures in other languages, as the core concept of the
closure type in A4 is that the content is directly equivalent to an A4 file.

(To be more precise - the standard call built-in,
"!call", loads the file as a closure using
"!importBJO".
"!call" accepts a closure and calls it.)

It is important to note that A4 doesn't (yet?) have direct support for receiving
closures, just the necessary symbol tools to perform the task manually.

This hasn't proven to be a problem at all in practice - the most difficult part
of using closures is the correct scoping to correctly propagate closure symbols
when inner closures are involved, and without loosening the scoping rules, this
is probably not avoidable.

Passing a closure as a call parameter instances it as per BBJOF conventions,
but this interacts in a complex way with scoping, as the closure is instanced
specifically during preparation of the call's parameters, and is instanced into
the working-area object.

A particular detail of this is that all parameter kinds, not just ordered
parameters, may be used to share arbitrary symbols between the called object
and specific closure instances.

On the receiving end of a closure, the integration of the closure into the
control flow and placing symbols to cause closure instancing must be done
manually.
Again, please read the conventions document.

It may be advisable to have
.BR bbjof-dump (1)
on-hand if things get particularly complicated.

It is important to note that the matter of the closure tree
matching structure conventions is not coincidence, and usage of this is critical
to passing closures to other blocks without conflicts.

.SH A4 - COMPILER BUILT-INS

The A4 compiler has built-in reference information on compiler built-ins.

However, there are a few specific details not mentioned explicitly, and a few translations of note.

Firstly, the type names. These might not quite be standardized yet, but:

"void": This returns nothing.

"label": This may be any label. If it may have an offset is dependent on the
semantics of the built-in.

"int": The numeric value of this must be known, right now, without any lookup.
This is invariably internally a label to
"0" with an offset - that is the exact definition of a literal int.

"ind.int": Ordinary ints are a more boring subset of indirect ints.
Something is an indirect int if, and ONLY if, at the exact time
that this state is checked, there is a chain of imports and exports leading to a
"0"-plus-offset pair for the given name in the current block, or a parent block
AST-wise. 

Note that the logic that searches parent blocks completely ignores alias maps.

As such, for obvious safety and sanity reasons, this is only used by compiler
built-ins that, previously, would always error if not given numeric literals.

Also note that since, for obvious efficiency reasons, a !repeat's block is
compiled and *then* duplicated, the parameters provided by !repeat are NOT
resolvable under these rules.

Secondly, there's the translations.

During parsing, complex address sources are converted to calls to
"!complex".

During compilation, calls to
":" are replaced with calls to
"equ".

It's important to note that
"=" is a compiler built-in. It may be made more flexible in future to allow the
copying of structures.

Finally, calls to things that are not compiler built-ins are turned into, say,
"!call(!importBJO(name), ...)" - this is important to note if modifying A4 to
include a more convenient way to instance closures, for example, as it implies
that the internal architectural work is already somewhat done.
(The primary problem is that
"!call" is not as flexible as closure instancing.)

.SH A4 - LIBRARY2

Original (non-/B, non-Nim-based) A4 supported a pre-processor.

Original (non-/B, non-Nim-based) A4 was awful as a result of overreliance on
said pre-processor to emulate things now done via closures.

A4/B is now quite incompatible with original A4. It's mostly binary-compatible.

Keep in mind that A4 is intended to be self-sufficient with only the output of
the
.BR bbjof-gentbls (1)
program. The standard library is a set of useful resources, but the A4 compiler
is NOT allowed to depend on it - or really anything that isn't being used as a
way to avoid having to pass the A4 compiler machine-specific information.

This in mind, rather than documenting library2, it's better to just read it.

The worst outcome of this is that you have to learn more about how it works,
which is a good idea if intending to write ByteByteJump code using any language.

.SH "SEE ALSO"
.BR bbjof (5)
.BR bbjof-conventions (7)
.BR bbjof-a4 (1)

.SH AUTHOR
20kdc
