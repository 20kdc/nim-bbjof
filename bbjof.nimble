# Package

version       = "0.1.0"
author        = "20kdc"
description   = "Nim implementation of BBJOF (ByteByteJump Object Format) IO and tooling"
license       = "CC0-1.0"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["bbjof"]

# Dependencies

requires "nim >= 1.2.0"
requires "bio >= 0.1.0"
requires "npeg == 0.22.2"
