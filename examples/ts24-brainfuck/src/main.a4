/*
 * TS24 BRAINFUCK INTERPRETER
 * proof of bounded-storage-machine status
 */

programHeader: !alloc(9, 0, 0, 1);
stdio: !alloc(1, 0xFFFFFF, 0, 1);

// 'All remaining space' is the ideal here.
vmMemory: !alloc(0xFEFFFF, 0, 0x10000, 1);

// Tape (though this is also used during program read-in).
byte(tape.2, vmMemory$2);
byte_bss(tape.1);
byte_bss(tape.0);
byte(ip.2, vmMemory$2);
byte_bss(ip.1);
byte_bss(ip.0);

// Use an address for the loop depth counter
byte_bss(loopDepth.2);
byte_bss(loopDepth.1);
byte_bss(loopDepth.0);

byte_bss(instruction);

!attachReloc(_start, 2, programHeader + 6);
!attachReloc(_start, 1, programHeader + 7);
!attachReloc(_start, 0, programHeader + 8);

// Maths pages
page_inc(page_inc);
page_dec(page_dec);
page_c_zero(page_c_zero);

// Equality check temp
page_c_zero(equalityPage);
!attachReloc(false, 0, equalityPage);

// The program begins...
_start;

// Read-in
loop({
  !alias(@break, break);
  instruction = stdio;
  addressWrite(tape, instruction);
  addressInc(tape);
  // '!' separates code and data
  ifEq(instruction, 33, {
    goto(break);
  });
});

// The program has been read. Begin!
loop({
  !alias(@break, break);
  addressRead(ip, instruction);

  // Instructions...

  // '<'
  ifEq(instruction, 60, {
    addressDec(tape);
    goto(instrEnd);
  });
  // '>'
  ifEq(instruction, 62, {
    addressInc(tape);
    goto(instrEnd);
  });
  // '-'
  ifEq(instruction, 45, {
    addressRead(tape, data);
    addressWrite(tape, page_dec[%0^data]);
    goto(instrEnd);
  });
  // '+'
  ifEq(instruction, 43, {
    addressRead(tape, data);
    addressWrite(tape, page_inc[%0^data]);
    goto(instrEnd);
  });

  // '['
  ifEq(instruction, 91, {
    addressRead(tape, data);
    if(page_c_zero[%0^data], {
      // if the cell is zero, this must go to the matching ]
      loop({
        !alias(@break, break);
        // implicitly skips this '['
        addressInc(ip);
        addressRead(ip, instruction);
        // '[' increments depth
        ifEq(instruction, 91, { addressInc(loopDepth); });
        // ']' decrements depth
        ifEq(instruction, 93, {
          if(page_c_zero[loopDepth.0], {
            if(page_c_zero[loopDepth.1], {
              if(page_c_zero[loopDepth.2], {
                goto(break);
              });
            });
          });
          addressDec(loopDepth);
        });
      });
      // the auto-increment at instrEnd goes from the ] to the instruction after
      // which is fine
    });
    goto(instrEnd);
  });
  // ']'
  ifEq(instruction, 93, {
    addressRead(tape, data);
    ifNot(page_c_zero[%0^data], {
      // if the cell is not zero, this must return to the matching [
      loop({
        !alias(@break, break);
        // implicitly skips this ']'
        addressDec(ip);
        addressRead(ip, instruction);
        // ']' increments depth
        ifEq(instruction, 93, { addressInc(loopDepth); });
        // '[' decrements depth
        ifEq(instruction, 91, {
          if(page_c_zero[loopDepth.0], {
            if(page_c_zero[loopDepth.1], {
              if(page_c_zero[loopDepth.2], {
                goto(break);
              });
            });
          });
          addressDec(loopDepth);
        });
      });
      // the auto-increment at instrEnd goes from the [ to the instruction after
      // which is fine
    });
    goto(instrEnd);
  });

  // ','
  ifEq(instruction, 44, {
    addressWrite(tape, stdio);
    goto(instrEnd);
  });
  // '.'
  ifEq(instruction, 46, {
    addressRead(tape, stdio);
    goto(instrEnd);
  });

  // '!'
  ifEq(instruction, 33, {
    goto(break);
  });

  instrEnd;
  addressInc(ip);

});

// program exited
goto(stdio);
