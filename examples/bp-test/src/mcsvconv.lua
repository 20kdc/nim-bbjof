-- hastily-written script
-- this is only included in case it helps
-- converts the output of midicsv to A4 sources
-- doesn't bother with tempo though, see next line
local function velMod(vel)
  -- pre-amp
  -- vel = ((vel - 127) / 2) + 127
  -- adjust to taste ;)
  return vel * 0.4 -- 20kdc - lunarmenu
  -- return vel * 0.125 -- NORMAL stuff
  -- return vel * 0.0625 -- LESS NORMAL stuff
  -- (SOMEWHERE EVEN CLOSER TO ZERO) -- any MIDI above 1MB
end
print("!alias(@0, time);")
print("!alias(@1, note);")

print("goto(pastWorkspace);")
print("note.@return;")
print("instruction(0, 0, 0[%0^noteReturnAddress, %0, %0]);")
print("pastWorkspace;")

local labelNum = 0
local function genLabel()
  labelNum = labelNum + 1
  return "temp" .. labelNum
end

local maxTime = 0

local eventsAtTimes = {}
local times = {}

-- This gets updated based on tempo.
-- It's the amount one *multiplies* a MIDI tick by to get the amount of BytePusher frames.
local ticksToFrames = 1
local fileDivision = 1

local function insertEvent(time, fn)
  if not eventsAtTimes[time] then
    eventsAtTimes[time] = {}
    table.insert(times, time)
  end
  table.insert(eventsAtTimes[time], fn)
end

while true do
  local fld = io.read()
  if fld == nil then break end
  fld = fld:gmatch("[^ ,]+")
  fld()
  local time = tonumber(fld())
  time = math.floor(time * ticksToFrames)
  maxTime = math.max(maxTime, time)
  local tp = fld()
  if tp == "Header" then
    fld()
    fld()
    fileDivision = tonumber(fld())
  elseif tp == "Tempo" then
    -- Microseconds per beat.
    -- Beats are then divided by fileDivision to get ticks.
    local ticksToSeconds = (tonumber(fld()) / 1000000) / fileDivision
    ticksToFrames = ticksToSeconds * 60
  elseif tp == "Note_on_c" then
    if fld() == "9" then
      -- percussion, do nothing for now
    else
      -- note!!!
      local note = tonumber(fld())
      local vel = math.floor(velMod(tonumber(fld())))
      local relNote = (note - 69) / 12
      local hz = 440 * (2^relNote)
      local halfPeriod = math.floor((15360 / (hz * 2)) + 0.5)
      if halfPeriod < 0 then halfPeriod = 0 end
      if halfPeriod > 0xFF then halfPeriod = 0xFF end
      insertEvent(time, {"NOTE", halfPeriod, vel})
    end
  end
end

insertEvent(maxTime, {"END"})

local eventHigh, eventLow
local eventHighTerminator, eventLowTerminator
local function event(high, low)
  if eventHigh ~= high then
    if eventLow then
      event(eventHigh, nil)
    end
    if eventHigh then
      print(eventHighTerminator .. ";")
    end
    if high then
      eventHighTerminator = genLabel()
      print("ifNot (bank_c_eq[time.1, %" .. high .. "], { goto(" .. eventHighTerminator .. "); });")
    end
    eventHigh = high
  end
  if eventLow ~= low then
    if eventLow then
      print("goto(completed);")
      print("" .. eventLowTerminator .. ";")
    end
    if low then
      eventLowTerminator = genLabel()
      print("ifNot (bank_c_eq[time.0, %" .. low .. "], { goto(" .. eventLowTerminator .. "); });")
    end
    eventLow = low
  end
end

table.sort(times)
for _, v in ipairs(times) do
  event(v >> 8, v & 0xFF)
  local notesByHP = {}
  for _, ev in ipairs(eventsAtTimes[v]) do
    if ev[1] == "NOTE" then
      notesByHP[ev[2]] = (notesByHP[ev[2]] or 0) + ev[3]
    elseif ev[1] == "END" then
      -- this gets wrapped - can't use 0 0 directly because that would miss
      -- the first tick
      print("time.0 = %255;")
      print("time.1 = %255;")
    else
      error("Wha?")
    end
  end
  for k, v in pairs(notesByHP) do
    print("mNote(" .. k .. "," .. v .. ");")
  end
end

event(nil, nil)
print("completed;")

