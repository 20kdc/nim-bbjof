# Example Makefile.
# Semi-obviously, you'd have to adjust this for your own personal environment.

BBJOF := ../../bbjof

LIBRARY2 := ../library2

.PHONY: run-bp run-ts clean

# Main compilation sequence (MUST BE FIRST)

main.bin: main.bjo
	$(BBJOF) ld $(BBJOF_LDFLAGS) --binary main.bjo --output:main.bin

main.bjo: obj/main.final.bjo
	$(BBJOF) ld $(BBJOF_LDFLAGS) obj/main.final.bjo --output:main.bjo

obj/main.final.bjo: obj/main.bjo obj/0.bjo obj
	$(BBJOF) merge obj/main.bjo obj/0.bjo --output:obj/main.final.bjo

# Utility targets

clean:
	rm -f main.bin main.bjo
	rm -f obj/*

run-bp: main.bin
	cp main.bin ../../jsbpvm/in.bp
	x-www-browser http://127.0.0.1:16000/ &
	python3 -m http.server --bind 127.0.0.1 -d ../../jsbpvm 16000

run-ts: main.bin
	$(BBJOF) ts $(BBJOF_MACHCFG) main.bin

# A4/B Incremental Compilation {

# main

obj/%.bjo: src/%.a4 obj/instruction.bjo obj
	$(BBJOF) a4 --path:obj $< --output:$@

obj/%.bjo.d: src/%.a4 obj
	$(BBJOF) a4 --makedeps:obj/$*.bjo $< --output:$@

# library2

obj/%.bjo: $(LIBRARY2)/%.a4 obj/instruction.bjo obj
	$(BBJOF) a4 --path:obj $< --output:$@

obj/%.bjo.d: $(LIBRARY2)/%.a4 obj
	$(BBJOF) a4 --makedeps:obj/$*.bjo $< --output:$@

# fallback

obj/%.bjo.d: obj
	touch $@

# }

# library2 Resource Compilation {

obj/kifont.bjo: $(LIBRARY2)/kifont.font.bmp obj
	$(BBJOF) bmp2bin --sprites $< --output:$@.1.bin
	$(BBJOF) bmp2bin --spacing $< --output:$@.2.bin
	$(BBJOF) binto --align:65536 --start-sym:kifont_sprites $@.1.bin --output:$@.1.bjo
	$(BBJOF) binto --align:256 --start-sym:kifont_spacing $@.2.bin --output:$@.2.bjo
	$(BBJOF) merge $@.1.bjo $@.2.bjo --output:$@.bjo

# }

# Resource Compilation {

obj/%.bjo: src/%.screen.bmp obj
	$(BBJOF) bmp2bin $< --output:$@.bin
	$(BBJOF) binto --align:65536 --start-sym:@0 $@.bin --output:$@

obj/%.bjo: src/%.sprites.bmp obj
	$(BBJOF) bmp2bin --sprites $< --output:$@.bin
	$(BBJOF) binto --align:65536 --start-sym:@0 $@.bin --output:$@

obj/%.bjo: src/%.sprites.1.bmp obj
	$(BBJOF) bmp2bin --sprites $< --output:$@.bin
	head -c 256 $@.bin | $(BBJOF) binto --align:256 --start-sym:@0 - --output:$@

# }

# 'obj' dir initialization

obj:
	mkdir obj

obj/instruction.bjo: obj $(BBJOF)
	$(BBJOF) gentbls $(BBJOF_MACHCFG) --output:obj

# Include root source file

include obj/main.bjo.d

