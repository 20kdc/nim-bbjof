# A4 / ByteByteJump Object Format
#  merge & link operations

import ../bbjof
import tables

proc mergeRef(target: BBJOF, r: BBJOFRef): BBJOFRef =
  if r.index < 0:
    # import
    result = BBJOFRef(
      index: r.index - len(target.imports),
      offset: r.offset
    )
  else:
    # alloc
    result = BBJOFRef(
      index: r.index + len(target.allocs),
      offset: r.offset
    )

proc merge*(target: BBJOF, source: BBJOF, location: string): void =
  ## Merges source into target.
  ## If a location is provided (i.e. is not empty), then debug symbols are modified accordingly.
  # Handle things that have refs first so that the forward lengths are correct
  for v in source.debugs:
    var resultName = v.name
    if location != "":
      resultName = location & "," & resultName
    target.debugs.add(BBJOFExp(
      name: resultName,
      target: target.mergeRef(v.target)
    ))
  for v in source.exports:
    target.exports.add(BBJOFExp(
      name: v.name,
      target: target.mergeRef(v.target)
    ))
  for v in source.relocs:
    target.relocs.add(BBJOFRel(
      addrByte: v.addrByte,
      source: target.mergeRef(v.source),
      target: target.mergeRef(v.target)
    ))
  # These two affect ref merging and have no refs, so must be handled last.
  target.allocs &= source.allocs
  target.imports &= source.imports
  target.sizeClass = bbjofSizeMax(target.sizeClass, source.sizeClass)

proc patchByte(alloc: var BBJOFAlloc, index: int, val: char): void =
  if alloc.data == "":
    raise newException(IOError, "Relocation to uninitialized data")
  if index < 0:
    raise newException(IOError, "Relocation to index < 0 (" & $index & ")")
  if index >= len(alloc.data):
    raise newException(IOError, "Relocation to index >= " & $len(alloc.data) & " (" & $index & ") - if using !putText or !attachReloc, check your indices and lengths")
  alloc.data[index] = val

proc linkExportsToImports*(target: BBJOF): void =
  ## DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING
  ## This only performs part of the linking process, use link()
  # Don't use newTableFrom, since that means the verification has to be done some other way.
  # Instead, do things manually so that the exports can be inline-verified,
  #  and use the export indices so -1 is a valid default, and so on.
  var exportNameMap = newTable[string, int]()
  for i, v in target.exports.pairs():
    if exportNameMap.contains(v.name):
      raise newException(IOError, "There are at least two exports with the name: " & v.name)
    else:
      exportNameMap.add(v.name, i)

  # Linking may have to be multi-pass to resolve chains of imports/exports.
  var doingWork = true
  var iterations = 0
  while doingWork:
    iterations += 1
    if iterations == 256:
      raise newException(IOError, "Hit 256 link propagation iterations. You probably broke something.")
    doingWork = false

    # Find resolvable imports.
    var importToExportMap = newTable[BBJOFInt, BBJOFInt]()
    for k, v in target.imports.pairs():
      var res = exportNameMap.getOrDefault(v, -1)
      if res != -1:
        importToExportMap.add(k, res)

    # Resolve the resolvable imports.
    # This process may change the resolutions to other imports, so import removal has to be deferred anyway.
    for r in target.refs(true):
      var res = importToExportMap.getOrDefault(r.importIdx(), -1)
      if res != -1:
        var tgref = target.exports[res].target
        if tgref.index == r.index:
          # this means we're definitely going around in circles
          raise newException(IOError, "Export " & target.exports[res].name & " exports itself. Check for infinite loops without filler instructions.")
        r.index = tgref.index
        r.offset += tgref.offset
        # since something updated references, need to run another pass
        doingWork = true

proc linkRelocations*(target: BBJOF): void =
  ## DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING
  ## This only performs part of the linking process, use link()
  # Done with linking passes.
  # And now the linear "apply relocations" pass.
  # This has to occur before stripping unused imports.
  var newRelocs: seq[BBJOFRel]
  for v in target.relocs:
    if v.target.index >= 0:
      # Target is valid. Check source.
      var sourceValid = false
      var sourceAt: BBJOFInt = 0
      if v.source.index < 0 and target.imports[v.source.importIdx()] == "0":
        # "0" symbol
        sourceValid = true
      elif v.source.index >= 0 and target.allocs[v.source.index].alignStep == 0:
        # Normal
        sourceAt = target.allocs[v.source.index].alignStart
        sourceValid = true
      # Alright, is it valid?
      if sourceValid:
        # The source allocation is fixed.
        # Let the patching commence!
        let val = sourceAt + v.source.offset
        let valChar = (char) ((val shr (v.addrByte * 8)) and 0xFF)
        target.allocs[v.target.index].patchByte((int) v.target.offset, valChar)
        # don't add reloc to newRelocs
        continue
    newRelocs.add(v)
  target.relocs = newRelocs

proc linkCleanImports*(target: BBJOF): void =
  ## DO NOT USE UNLESS YOU KNOW WHAT YOU ARE DOING
  ## This only performs part of the linking process, use link()
  # Decide on a canonical version of each import.
  var importNameMap = newTable[string, BBJOFInt]()
  var importCanonMap = newTable[BBJOFInt, BBJOFInt]()
  for k, v in target.imports.pairs():
    if not importNameMap.contains(v):
      importNameMap.add(v, k)
    importCanonMap.add(k, importNameMap[v])

  # Find all imports that are necessary, and also switch to canonical versions.
  var importsThatAreNecessary = newSeq[bool](len(target.imports))
  for r in target.refs(false):
    if r.index < 0:
      let effectiveImport = importCanonMap[r.importIdx()]
      r.setImportIdx(effectiveImport)
      importsThatAreNecessary[effectiveImport] = true

  # Remove debugs that point to imports that are going to be deleted.
  var newDebugs: seq[BBJOFExp]
  for v in target.debugs:
    let impIdx = v.target.importIdx()
    if impIdx >= 0:
      if not importsThatAreNecessary[impIdx]:
        continue
    newDebugs.add(v)
  target.debugs = newDebugs

  # And now remove the ones that aren't necessary, and remap the ones which are.
  var importRemap = newSeq[BBJOFInt](len(importsThatAreNecessary))
  var mapping: BBJOFInt = 0
  var newImports: seq[string]
  for k, v in importRemap.mpairs():
    if importsThatAreNecessary[k]:
      v = mapping
      mapping += 1
      newImports.add(target.imports[k])
    else:
      v = -1
  target.imports = newImports
  # Finish remapping.
  for r in target.refs(true):
    if r.index < 0:
      let val = importRemap[r.importIdx()]
      if val < 0:
        raise newException(IOError, "linkCleanImports is broken")
      r.setImportIdx(val)

proc link*(target: BBJOF): void =
  ## Links internal references in target. Throws IOError on duplicate export names.
  ## Specifically, this:
  ##  1. Links imports to exports
  ##  2. Links relocations
  ##  3. Optimizes imports (removes duplicates & unnecessary imports)
  ## Note that the use of all 3 stages are all relied upon in various bits of code. They're API.
  target.linkExportsToImports()
  target.linkRelocations()
  target.linkCleanImports()
