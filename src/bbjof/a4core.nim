# The reusable, complicated little bit of A4 that does the true work.
import ./mvsym
import ./merge
import ../bbjof
import tables
import strutils

type
  A4CoreSource* = object
    # A Source represents a constant which may have attachments.
    label*: string
    offset*: BBJOFInt
    # -1 means "don't extract"
    extract*: BBJOFInt
    attachments*: seq[string]

  A4CoreAdjustmentKind* = enum
    a4caVoid, # this is a default to help prevent errors
    a4caAddress,
    a4caClosure
  A4CoreAdjustment* = object
    # NOTE: Trivial adjustments are converted to Mvsyms of the *opposite direction*.
    # This does make perfect sense if the nature of the involved operations is understood,
    #  but it's something to be aware of.
    kind*: A4CoreAdjustmentKind
    # for 'address'
    # must have at least 1 element
    # this remains in lowest-to-highest byte order; last element is base
    source*: seq[A4CoreSource]
    # for 'closure'
    closure*: BBJOF

proc toA4CoreSource*(source: BBJOFMvsymTarget): A4CoreSource =
  A4CoreSource(label: source.name, offset: source.offset, extract: -1)

proc toA4CoreAdjustment*(source: BBJOFMvsymTarget): A4CoreAdjustment =
  A4CoreAdjustment(
    kind: a4caAddress,
    source: @[source.toA4CoreSource]
  )

proc addressOperateOnImport(adjustment: A4CoreAdjustment, result: BBJOF, theImport: int): void =
  ## This is the core of the core of A4/B.
  # This is a set of hastily-made imports, one per source.
  let newImportBase = len(result.imports)
  for src in adjustment.source:
    result.imports.add(src.label)
    if src.label == "":
      raise newException(IOError, "Complex byte source going to ''")
  # Alright, now engage warp drive.
  for k, reloc in result.relocs.mpairs():
    if reloc.source.importIdx() == theImport:
      # Warp. Get the source index and apply it.
      var sourceIdx = reloc.addrByte
      if reloc.addrByte >= len(adjustment.source):
        sourceIdx = len(adjustment.source) - 1
      let src = adjustment.source[sourceIdx]
      reloc.source.setImportIdx(newImportBase + sourceIdx)
      reloc.source.offset += src.offset
      if src.extract != -1:
        reloc.addrByte = src.extract
      # And handle attachments.
      for attach in src.attachments:
        result.exports.add(BBJOFExp(
          target: reloc.target,
          name: attach
        ))
  # Cleanup time.
  # Debug symbols? Delete 'em.
  var debugsToRemove: seq[int]
  for k, v in result.debugs.pairs():
    if v.target.importIdx() == theImport:
      debugsToRemove.add(k)
  result.debugs.bbjofDeleteSortedIndices(debugsToRemove)
  # If there are any references remaining, they weren't handled properly. PANIC IMMEDIATELY.
  for v in result.refs(false):
    if v.importIdx() == theImport:
      raise newException(IOError, "ref remaining to '" & result.imports[theImport] & "' (you tried something invalid)")

proc addressApply(adj: A4CoreAdjustment, obj: BBJOF, target: string): void =
  # We can't apply this to exports. Complain.
  for k, v in obj.exports.pairs():
    if v.name == target:
      raise newException(IOError, "Attempted to modify export '" & v.name & "'")
  # And finally, imports.
  var theImport: int = -1
  for k, v in obj.imports.pairs():
    if v == target:
      theImport = k
      break
  if theImport != -1:
    # Right, so, we want to delete the import,
    #  but that'd break references.
    # ... Let the linker remove the import, as it's under an obscure name.
    adj.addressOperateOnImport(obj, theImport)

proc closureApply(adj: A4CoreAdjustment, obj: BBJOF, target: string): void =
  # Collate symbols that could indicate closure instances from the object being worked on.
  var relevantSymbols: seq[string]
  let targetPrefix = target & "."
  for v in obj.imports:
    if v.startsWith(targetPrefix):
      relevantSymbols.add(v[len(targetPrefix)..len(v) - 1])
  for v in obj.exports:
    if v.name.startsWith(targetPrefix):
      relevantSymbols.add(v.name[len(targetPrefix)..len(v.name) - 1])
  # Now that all symbols have been added, determine the set of closure instances.
  var instances = newTable[string, bool]()
  for v in relevantSymbols:
    let res = v.bbjofGetClosureInstance
    if res.ok:
      if not instances.hasKey(res.name):
        instances[res.name] = true
  # Finally, need to actually create closure instances.
  # This amounts to a very special-case symbol rename,
  #  to add the prefix to everything.
  for instName in instances.keys():
    var inst = deepCopy(adj.closure)
    proc renamer(v: var string, iName: string): void =
      if v == "":
        raise newException(IOError, "closure instancer got empty-string symbol")
      elif v[0] == '@':
        v = targetPrefix & iName & v
    for v in inst.imports.mitems():
      renamer(v, instName)
    for v in inst.exports.mitems():
      renamer(v.name, instName)
    obj.merge(inst, instName)

proc applyA4Core*(obj: BBJOF, stmt: TableRef[string, A4CoreAdjustment]): void =
  ## This is the core of A4/B.
  ## It's really more like an extended form of mvsym.
  ## It uses mvsym where reasonable to take advantage of structures,
  ##  and otherwise does it's own thing.

  var coreVarCounter = 0
  # Rename all targetted symbols into a set of "working area" positions.
  # The ones that can be trivially renamed are immediately renamed.
  var adjustTableA = newTable[string, BBJOFMvsymTarget]()
  var adjustTableB = newTable[string, BBJOFMvsymTarget]()
  var complex = newTable[string, A4CoreAdjustment]()

  for target, adj in stmt.pairs():
    # AHEM. If you came here looking for advice from the guy who wrote this about your:
    # src/main.a4(19, 1): During apply of 'loopUntil': Attempted to delete import ' A4CORE_HOLDING_PEN_3.@1.@0'
    # error, then there are two possibilities.
    # 1. Someone broke something.
    # 2. *Something isn't exporting a label that your closure expects.*
    #     In this case, it would be 'loopUntil',
    #      and the cause of the offense would be that condition.@0 (or @1.@0)
    #      wasn't escaping the inner loop block because of a missed !global.
    let newName = " A4CORE_HOLDING_PEN_" & $coreVarCounter & "." & target
    coreVarCounter += 1
    adjustTableA[target] = BBJOFMvsymTarget(name: newName)
    if adj.kind == a4caAddress and len(adj.source) == 1 and len(adj.source[0].attachments) == 0 and adj.source[0].extract == -1:
      adjustTableB[newName] = BBJOFMvsymTarget(name: adj.source[0].label, offset: adj.source[0].offset)
    else:
      # This rule ensures that the complex adjustment MUST bind to everything
      #  which is important to localize errors that would otherwise occur
      #  along with preventing a leak of the holding-pen vars,
      #  which CANNOT be allowed to happen (two different holding-pen sets mix, and "boom")
      adjustTableB[newName] = BBJOFMvsymTarget(name: "")
      complex.add(newName, adj)

  # Put symbols into holding pens.
  # This is critical to preventing the later symbol rename causing overlap issues.
  obj.mvsym(adjustTableA, bbjofmvmNormal)

  # Link now for two reasons:
  # 1. This deals with overlapping import/export pairs
  # 2. This ensures there's only one import per adjustment max
  obj.link()

  # Perform complex adjustments.
  for target, adj in complex.pairs():
    if adj.kind == a4caAddress:
      adj.addressApply(obj, target)
    elif adj.kind == a4caClosure:
      adj.closureApply(obj, target)
    else:
      raise newException(IOError, "NYI adjustment kind")

  # This has to be done because adjustTableB eliminates the holding pen labels
  obj.link()

  # Now that everything's safely handled, give all the symbols their final names
  obj.mvsym(adjustTableB, bbjofmvmNormal)
