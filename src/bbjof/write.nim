# A4 / ByteByteJump Object Format
#  writer

import ../bbjof
import streams
import bio

# internal: write a BBJOFInteger
proc writeBBJOFInt(stream: Stream, value: BBJOFInt, sc: BBJOFSizeClasses): void =
  if sc == bbjofsc32:
    stream.write(bio.serialize((uint32) value, bigEndian))
  elif sc == bbjofsc64:
    stream.write(bio.serialize((uint64) value, bigEndian))
  else:
    raise newException(IOError, "Invalid type class")

proc writeBBJOFStr(stream: Stream, value: string, sc: BBJOFSizeClasses): void =
  stream.writeBBJOFInt(len(value), sc)
  stream.write(value)

proc writeBBJOFRef(stream: Stream, value: BBJOFRef, sc: BBJOFSizeClasses): void =
  stream.writeBBJOFInt(value.index, sc)
  stream.writeBBJOFInt(value.offset, sc)

proc writeBBJOFExp(stream: Stream, value: BBJOFExp, sc: BBJOFSizeClasses): void =
  stream.writeBBJOFRef(value.target, sc)
  stream.writeBBJOFStr(value.name, sc)

proc writeBBJOFAlloc(stream: Stream, value: BBJOFAlloc, sc: BBJOFSizeClasses): void =
  if len(value.data) == 0:
    # negate (uninitialized)
    stream.writeBBJOFInt(-value.size, sc)
  elif len(value.data) != value.size:
    raise newException(IOError, "cannot have alloc with len(data) != size")
  else:
    # normal (initialized)
    stream.writeBBJOFInt(value.size, sc)
  stream.writeBBJOFInt(value.alignStart, sc)
  stream.writeBBJOFInt(value.alignStep, sc)
  stream.write(value.data)

proc writeBBJOFRel(stream: Stream, value: BBJOFRel, sc: BBJOFSizeClasses): void =
  stream.writeBBJOFRef(value.source, sc)
  stream.writeBBJOFInt(value.addrByte, sc)
  stream.writeBBJOFRef(value.target, sc)

proc writeBBJOF*(stream: Stream, value: BBJOF): void =
  ## Attempts to write a BBJOF file. Can throw exceptions if something goes wrong.
  if value.sizeClass == bbjofsc32:
    stream.writeBBJOFInt(0x42424A4F, bbjofsc32)
  elif value.sizeClass == bbjofsc64:
    stream.writeBBJOFInt(0x42424A5A, bbjofsc32)
  else:
    raise newException(IOError, "unsupported size class")

  stream.writeBBJOFInt(len(value.imports), value.sizeClass)
  stream.writeBBJOFInt(len(value.exports), value.sizeClass)
  stream.writeBBJOFInt(len(value.debugs), value.sizeClass)
  stream.writeBBJOFInt(len(value.allocs), value.sizeClass)
  stream.writeBBJOFInt(len(value.relocs), value.sizeClass)
  
  # IMPORTS
  for val in value.imports:
    stream.writeBBJOFStr(val, value.sizeClass)
  # EXPORTS
  for val in value.exports:
    stream.writeBBJOFExp(val, value.sizeClass)
  # DBGSYMS
  for val in value.debugs:
    stream.writeBBJOFExp(val, value.sizeClass)
  # ALLOCS
  for val in value.allocs:
    stream.writeBBJOFAlloc(val, value.sizeClass)
  # RELOCS
  for val in value.relocs:
    stream.writeBBJOFRel(val, value.sizeClass)
