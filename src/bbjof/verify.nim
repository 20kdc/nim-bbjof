import ../bbjof

proc verifyRef(target: BBJOF, bref: BBJOFRef, location: string, locationIndex: int, relocTargetMode: bool): void =
  if bref.index < 0:
    # The maths here removes the 'subtract 1' in favour of changing >= to >.
    # Also note, a relocation can't be verified until it's anchored (yes, even if offset < 0!)
    if -bref.index > len(target.imports):
      raise newException(IOError, "Import index " & $((-bref.index) - 1) & " in " & location & " " & $locationIndex & " is out of range (" & $len(target.imports) & " imports).")
  else:
    if bref.index >= len(target.allocs):
      raise newException(IOError, "Alloc index " & $bref.index & " in " & location & " " & $locationIndex & " is out of range (" & $len(target.allocs) & " allocs).")
    if relocTargetMode:
      if bref.offset < 0:
        raise newException(IOError, "Offset of " & $bref.offset & " in " & location & " " & $locationIndex & " is under 0.")
      if bref.offset >= len(target.allocs[bref.index].data):
        raise newException(IOError, "Offset of " & $bref.offset & " in " & location & " " & $locationIndex & " is not within the allocation (" & $len(target.allocs[bref.index].data) & " bytes)")

proc verify*(target: BBJOF): void =
  ## Verifies a BBJOF file's integrity. Will throw an IOError if something is wrong.
  ## Necessary for safety of other operations if and only if bounds checking is off.
  for i, v in target.exports.pairs():
    target.verifyRef(v.target, "target of export", i, false)
    for j, v2 in target.exports.pairs():
      if i != j and v.name == v2.name:
        raise newException(IOError, "Export tokens " & $i & " and " & $j & " share the name \"" & v.name & "\".")
  for i, v in target.allocs.pairs():
    if len(v.data) != 0 and len(v.data) != v.size:
      raise newException(IOError, "Data is initialized and not equal to size.")
  for i, v in target.relocs.pairs():
    target.verifyRef(v.source, "source of relocation", i, false)
    target.verifyRef(v.target, "target of relocation", i, true)
    if v.addrByte < 0:
      raise newException(IOError, "Relocation " & $i & " has an address byte of " & $v.addrByte & ", which is negative.")
