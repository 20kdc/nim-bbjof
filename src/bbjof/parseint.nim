import ../bbjof

func parseBBJOFInt*(str: string): BBJOFInt =
  ## Parses a BBJOFInt.
  ## Raises ValueError on failure.
  ## Supports 64-bit values, DELIBERATELY IGNORES VALUE RANGE CHECKS,
  ##  supports hexadecimal.

  # Sign. Note that true means positive.
  var sign = true
  var hex = false
  var i = 0

  # Handle different prefixes in order, and length-check before each

  if len(str) == 0:
    raise newException(ValueError, "Empty string cannot be parsed as an int")

  if str[0] == '-':
    sign = false
    i = 1

  if (len(str) - i) == 0:
    raise newException(ValueError, "- does not alone make an int")

  if (len(str) - i) >= 2:
    if str[i] == '0' and str[i + 1] == 'x':
      hex = true
      i += 2

  if (len(str) - i) == 0:
    raise newException(ValueError, "Having an empty string after the base prefix is silly")
  
  result = 0
  while i < len(str):
    var val = 0
    if str[i] >= '0' and str[i] <= '9':
      val = ((int) str[i]) - (int) '0'
    elif str[i] >= 'a' and str[i] <= 'f' and hex:
      val = (((int) str[i]) - (int) 'a') + 0xA
    elif str[i] >= 'A' and str[i] <= 'F' and hex:
      val = (((int) str[i]) - (int) 'A') + 0xA
    else:
      raise newException(ValueError, "Invalid character for int")

    if hex:
      # hex
      result *= 16
    else:
      # decimal
      result *= 10

    if sign:
      # positive
      result += val
    else:
      # negative
      result -= val
    i += 1
