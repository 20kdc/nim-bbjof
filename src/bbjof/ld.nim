# A4 / ByteByteJump Object Format
#  the alloc/tobin operations

import ../bbjof
import algorithm
import tables
import hashes

type
  AllocationClass = object
    backwards: bool
    pos: BBJOFInt
    step: BBJOFInt
    size: BBJOFInt

proc hash(ac: AllocationClass): Hash =
  var h: Hash = 0
  if ac.backwards:
    h = h !& 1
  else:
    h = h !& 0
  h = h !& (int) ac.pos
  h = h !& (int) ac.step
  h = h !& (int) ac.size
  result = !$h

proc alloc*(obj: BBJOF, memory: int): void =
  ## Fixes all unfixed allocations (that is, where alignStep != 0, adjusts alignStart to avoid intersecting and sets alignStep = 0)
  ## Note that it's only safe to apply this to one continuous "line" of objects.
  ## And all allocations that start fixed (i.e. machine headers) MUST be in the object before this is run.
  ## Importing fixed allocations into such a branch after alloc has been applied risks creating an invalid file.

  # Obvious sanity check
  if memory > 0x1000000:
    obj.sizeClass = bbjofsc64

  # This is a list of all the allocation indices that need fixing, sorted by the required order.
  var unfixed: seq[BBJOFInt] = @[]

  var allocmap: seq[bool] = newSeq[bool](memory)

  for k, v in obj.allocs.mpairs():
    if v.size == 0:
      # If an allocation has a size of 0, it is trivial to make fixed.
      # It also doesn't go into the fixed bucket because that would waste time.
      v.alignStep = 0
    elif v.alignStep == 0:
      for i in v.alignStart .. (v.alignStart + (v.size - 1)):
        allocmap[i] = true
    else:
      # This allocation is unfixed.
      unfixed.add(k)

  # Use this trivial algorithm for now. It runs in sane time bounds and generates close-to-optimal results.
  unfixed.sort do (a, b: BBJOFInt) -> int:
    # Note the flipped order.
    # Large alignment steps (higher "awkwardness") before smaller alignment steps.
    # This is the most critical part.
    result = cmp(obj.allocs[b].alignStep, obj.allocs[a].alignStep)

  # Allocation accelerator.
  # This caches the last successful allocation,
  #  which will cause only a single failed attempt rather than a lot of failures to occur.
  # Since there are a LOT of identical allocations (instructions) in a BytePusher program,
  #  this is very useful.
  var allocCache = newTable[AllocationClass, BBJOFInt]()

  # Right, time to actually make it happen.
  for id in unfixed:
    var pos: BBJOFInt = obj.allocs[id].alignStart
    var step: BBJOFInt = obj.allocs[id].alignStep
    var size: BBJOFInt = obj.allocs[id].size

    var aClass = AllocationClass(
      backwards: obj.allocs[id].data == "",
      pos: pos,
      step: step,
      size: size
    )

    if aClass.backwards:
      # Backwards
      pos += memory - (memory mod step)
      step = -step
      # Move back in-range
      while pos + size > memory:
        pos += step

    # Now that the setup's complete, check if this has been done before
    if allocCache.hasKey(aClass):
      pos = allocCache[aClass]

    while true:
      var fail = false
      # check to see if we hit the other side
      if step > 0:
        if pos + size > memory:
          fail = true
      else:
        if pos < 0:
          fail = true
      # error if so (failed to allocate)
      if fail:
        let allocDetail = " (pos=" & $pos & " step=" & $step & " size=" & $size & ")"
        raise newException(IOError, "Insufficient memory in target device for allocation" & allocDetail)
      # scan
      for i in pos .. (pos + (size - 1)):
        if allocmap[i]:
          fail = true
          break
      if not fail:
        break
      # ok, try again!
      pos += step

    # Now that a location is known, mark & insert the allocation as fixed.
    obj.allocs[id].alignStart = pos
    obj.allocs[id].alignStep = 0
    for i in pos .. (pos + (size - 1)):
      allocmap[i] = true

    # And also log this in the cache.
    allocCache[aClass] = pos

proc toBinary*(obj: BBJOF, force: bool): string =
  ## Assuming that uninitialized data does not need to be in the file,
  ## creates a contiguous array starting from 0 to the end of the
  ## initialized area containing all the initialized data (and zeroes in the holes)
  ## Trailing zeroes are removed.
  if len(obj.relocs) != 0 and not force:
    var possibilities = ""
    for v in obj.imports:
      possibilities &= "\n\tImport " & v
    raise newException(IOError, "Unresolved relocations" & possibilities)
  # Determine result size
  var size = 0
  for k, alloc in obj.allocs.pairs():
    if alloc.alignStep != 0 and not force:
      raise newException(IOError, "Unstable allocation #" & $k)
    # Only initialized data counts
    if alloc.data != "":
      if alloc.alignStart < 0:
        raise newException(IOError, "Allocation with non-empty data that starts before start of file #" & $k)
      let endV = alloc.alignStart + len(alloc.data)
      if endV > size:
        size = (int) endV
  result = newString(size)
  for i in 0 .. (size - 1):
    result[i] = (char) 0
  for alloc in obj.allocs:
    if alloc.alignStep == 0:
      for i in 0 .. (len(alloc.data) - 1):
        result[alloc.alignStart + i] = alloc.data[i]
  # Strip trailing zeroes
  var endWithoutZeroPadding = -1
  for i in 1 .. len(result):
    var res = len(result) - i
    if result[res] != (char) 0:
      endWithoutZeroPadding = res
      break
  result = result[0 .. endWithoutZeroPadding]

