# A4 / ByteByteJump Object Format
#  reader

import ../bbjof
import streams
import bio

# internal: read a BBJOFInteger
proc readBBJOFInt(stream: Stream, sc: BBJOFSizeClasses): BBJOFInt =
  if sc == bbjofsc32:
    result = stream.readBEInt32()
  elif sc == bbjofsc64:
    result = stream.readBEInt64()
  else:
    raise newException(IOError, "Invalid type class")

proc readBBJOFStr(stream: Stream, sc: BBJOFSizeClasses): string =
  result = stream.readStr((int) stream.readBBJOFInt(sc))

proc readBBJOFRef(stream: Stream, sc: BBJOFSizeClasses): BBJOFRef =
  result = BBJOFRef()
  result.index = stream.readBBJOFInt(sc)
  result.offset = stream.readBBJOFInt(sc)

proc readBBJOFExp(stream: Stream, sc: BBJOFSizeClasses): BBJOFExp =
  result = BBJOFExp()
  result.target = stream.readBBJOFRef(sc)
  result.name = stream.readBBJOFStr(sc)

proc readBBJOFAlloc(stream: Stream, sc: BBJOFSizeClasses): BBJOFAlloc =
  result = BBJOFAlloc()
  let sizeRaw = stream.readBBJOFInt(sc)
  result.alignStart = stream.readBBJOFInt(sc)
  result.alignStep = stream.readBBJOFInt(sc)
  if sizeRaw > 0:
    result.size = sizeRaw
    result.data = stream.readStr((int) sizeRaw)
  else:
    result.size = -sizeRaw
    result.data = ""

proc readBBJOFRel(stream: Stream, sc: BBJOFSizeClasses): BBJOFRel =
  result = BBJOFRel()
  result.source = stream.readBBJOFRef(sc)
  result.addrByte = stream.readBBJOFInt(sc)
  result.target = stream.readBBJOFRef(sc)

proc readBBJOF*(stream: Stream): BBJOF =
  ## Attempts to read a BBJOF file. Can throw exceptions when stuff goes wrong.
  result = BBJOF()
  let classID = readBEUint32(stream)
  if classID == 0x42424A4F:
    result.sizeClass = bbjofsc32
  elif classID == 0x42424A5A:
    result.sizeClass = bbjofsc64
  else:
    raise newException(IOError, "bbjof file was not ByteByteJump Object")

  result.imports = newSeq[string](stream.readBBJOFInt(result.sizeClass))
  result.exports = newSeq[BBJOFExp](stream.readBBJOFInt(result.sizeClass))
  result.debugs = newSeq[BBJOFExp](stream.readBBJOFInt(result.sizeClass))
  result.allocs = newSeq[BBJOFAlloc](stream.readBBJOFInt(result.sizeClass))
  result.relocs = newSeq[BBJOFRel](stream.readBBJOFInt(result.sizeClass))
  
  # IMPORTS
  var i = 0
  while i < len(result.imports):
    result.imports[i] = stream.readBBJOFStr(result.sizeClass)
    i += 1
  # EXPORTS
  i = 0
  while i < len(result.exports):
    result.exports[i] = stream.readBBJOFExp(result.sizeClass)
    i += 1
  # DBGSYMS
  i = 0
  while i < len(result.debugs):
    result.debugs[i] = stream.readBBJOFExp(result.sizeClass)
    i += 1
  # ALLOCS
  i = 0
  while i < len(result.allocs):
    result.allocs[i] = stream.readBBJOFAlloc(result.sizeClass)
    i += 1
  # RELOCS
  i = 0
  while i < len(result.relocs):
    result.relocs[i] = stream.readBBJOFRel(result.sizeClass)
    i += 1
