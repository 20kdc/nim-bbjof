# A4 / ByteByteJump Object Format
#  primitive objects

import ../bbjof
import sugar

type
  BBJOFMachineInfo* = object
    addrBytes*: int
    littleEndian*: bool

func newBBJOFPrim0*(ifo: BBJOFMachineInfo): BBJOF =
  ## Creates the '0' primitive.
  result = BBJOF()
  result.exports = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: "0"
    ),
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 256 - (4 * ifo.addrBytes)
      ),
      name: "false"
    ),
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 256 - (3 * ifo.addrBytes)
      ),
      name: "true"
    )
  ]
  result.allocs = @[
    BBJOFAlloc(
      size: 0,
      alignStart: 0,
      alignStep: 0,
      data: ""
    )
  ]

func newBBJOFPrimPipe*(imp: string, exp: string): BBJOF =
  ## Creates the 'pipe' primitive.
  result = BBJOF()
  result.imports = @[imp]
  result.exports = @[
    BBJOFExp(
      target: BBJOFRef(
        index: -1,
        offset: 0
      ),
      name: exp
    )
  ]

func newBBJOFPrimBSS*(size: int, align: int): BBJOF =
  ## Creates a BSS primitive.
  result = BBJOF()
  result.exports = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: "@0"
    )
  ]
  result.debugs = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: "@0"
    )
  ]
  result.allocs = @[
    BBJOFAlloc(
      size: size,
      alignStart: 0,
      alignStep: align,
      data: ""
    )
  ]

func newBBJOFPrimByte*(): BBJOF =
  ## Creates the 'byte' primitive.
  result = BBJOF()
  result.imports = @[
    "@1"
  ]
  result.exports = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: "@0"
    )
  ]
  result.debugs = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: "@0"
    )
  ]
  result.allocs = @[
    BBJOFAlloc(
      size: 1,
      alignStart: 0,
      alignStep: 1,
      data: "\x00"
    )
  ]
  result.relocs = @[
    BBJOFRel(
      source: BBJOFRef(
        index: -1,
        offset: 0
      ),
      addrByte: 0,
      target: BBJOFRef(
        index: 0,
        offset: 0
      )
    )
  ]

func newBBJOFPrimInstruction*(ifo: BBJOFMachineInfo, trampoline: bool): BBJOF =
  ## Creates the 'instruction' / 'cbtrampoline' primitives.
  result = BBJOF()
  var exportName: string
  if not trampoline:
    # instruction has 3 args (src, dst, jmp)
    exportName = "@call"
    result.imports = @[
      "@0",
      "@1",
      "@2"
    ]
  else:
    # trampoline has 2 args (false, true)
    # and they're actually @1, @2 respectively
    exportName = "@0"
    result.imports = @[
      "@1",
      "@2"
    ]

  result.exports = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: exportName
    )
  ]
  result.debugs = @[
    BBJOFExp(
      target: BBJOFRef(
        index: 0,
        offset: 0
      ),
      name: exportName
    )
  ]

  var addrCount: int = 3
  if trampoline:
    # trampoline has 4 fields
    addrCount = 4
  var data = ""
  # it's important to have this loop configuration so that the reloc order is easier to read,
  #  even if it makes this compile a tad slower (it's a gentbls routine, who cares lol)
  for y in 0 .. (addrCount - 1):
    for x in 0 .. (ifo.addrBytes - 1):
      var tfAB = x
      if not ifo.littleEndian:
        tfAB = ifo.addrBytes - (x + 1)
      # instruction just writes out params as-is
      var idx = -(y + 1)
      if trampoline:
        # trampoline has only 2 params and uses the first most of the time
        idx = -1
        if y == 3:
          idx = -2
      var byte_idx = (y * ifo.addrBytes) + x
      data.add("\x00")
      result.relocs.add(BBJOFRel(
        source: BBJOFRef(
          index: idx,
          offset: 0
        ),
        addrByte: tfAB,
        target: BBJOFRef(
          index: 0,
          offset: byte_idx
        )
      ))
  # instruction has no special alignment requirements
  result.allocs = @[
    BBJOFAlloc(
      size: len(data),
      alignStart: 0,
      alignStep: 1,
      data: data
    )
  ]
  if trampoline:
    # trampoline needs 256-alignment and needs to be at the end of a page
    result.allocs[0].alignStart = 256 - len(data)
    result.allocs[0].alignStep = 256

proc addExportAndDebug(obj: var BBJOF, name: string, r: BBJOFRef): void =
  let exp = BBJOFExp(
    target: r,
    name: name
  )
  obj.exports.add(exp)
  obj.debugs.add(exp)

func newBBJOFPrimBinary*(size: int, alignStart: BBJOFInt, alignStep: BBJOFInt, data: (int) -> uint8, startSym: string, endSym: string, lenSym: string): BBJOF =
  ## Creates a binary.
  result = BBJOF()
  if startSym != "":
    result.addExportAndDebug(startSym, BBJOFRef(index: 0, offset: 0))
  if endSym != "":
    result.addExportAndDebug(endSym, BBJOFRef(index: 0, offset: size))
  if lenSym != "":
    result.imports.add("0")
    result.addExportAndDebug(lenSym, BBJOFRef(index: -1, offset: size))
  var dataSeq = newString(size)
  for k in 0 .. (len(dataSeq) - 1):
    dataSeq[k] = (char) data(k)
  result.allocs = @[
    BBJOFAlloc(
      size: size,
      alignStart: alignStart,
      alignStep: alignStep,
      data: dataSeq
    )
  ]

func newBBJOFPrimBank*(data: (uint8, uint8) -> uint8): BBJOF =
  ## Creates a bank.
  result = newBBJOFPrimBinary(65536, 0, 65536, (pos) => data((uint8) (pos shr 8), (uint8) (pos)), "@0", "", "")

func newBBJOFPrimPage*(data: (uint8) -> uint8): BBJOF =
  ## Creates a page.
  result = newBBJOFPrimBinary(256, 0, 256, (pos) => data((uint8) (pos)), "@0", "", "")

func newBBJOFPrimIdFromDecInc*(inc: bool): BBJOF =
  ## Creates the 'id_from_page_dec' / 'id_from_page_inc' primitives.
  result = BBJOF()
  var ofs = 1
  if not inc:
    result.imports = @["page_dec"]
  else:
    result.imports = @["page_inc"]
    ofs = -1
  for i in 0 .. 255:
    result.exports.add(
      BBJOFExp(
        target: BBJOFRef(
          index: -1,
          offset: (i + ofs) and 0xFF
        ),
        name: "id." & $i
      )
    )
