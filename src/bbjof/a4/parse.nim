import ./lex

# The A4/B syntax itself is deliberately very simple.
# The compiler is meant to provide a LOT of services.
# No matter how case-specific.

type
  A4ElementKind* = enum
    a4eAtom, # someSymbol
    a4eCall, # someSymbol(<...>) | X someSymbol Y
    a4eClosure, # { <block content> }
    a4eError # Indicates there's a parse error. The parser tries to recover.
  A4Element* = ref object
    position*: A4Position # same error-reporting business as usual. universal.
    kind*: A4ElementKind
    name*: string # atom, error, call
    elements*: seq[A4Element] # call, closure

# Utilities.

proc a4parseGetAllErrors*(at: seq[A4Element]): seq[A4Element] =
  for v in at:
    if v.kind == a4eError:
      result.add(v)
    if v.kind == a4eCall or v.kind == a4eClosure:
      result &= a4parseGetAllErrors(v.elements)

func a4parseHasInterruption(tokens: seq[A4Token]): bool =
  var depth = 0
  for v in tokens:
    depth += v.depth()
    if depth < 0:
      return true
  return false

func a4parseGetStatements(tokens: seq[A4Token], split: string): seq[seq[A4Token]] =
  ## This is the outermost parsing step for each block.
  ## Given a seq[A4Token], breaks it up into the components.
  ## This is NOT just "split by ;" as it has to consider nesting.
  ## It's the authoritative reference on statement splitting, which conveniently isolates errors.
  ## Note that this does NOT care about mismatched tokens.
  ## Again, that would break isolation.
  var building: seq[A4Token] = @[]
  var depth: int = 0
  for v in tokens:
    if v.text == split and depth == 0:
      result.add(building)
      building = @[]
    else:
      if split == ";":
        # this little special-case improves error accuracy
        depth += v.depthClosure()
      else:
        depth += v.depth()
      building.add(v)
  result.add(building)

# Forward declarations of the parsers,
#  because of recursion and nice ordering
func a4parseBlockCore(tokens: seq[A4Token], split: string): seq[A4Element]
func a4parseElement(fallbackPos: A4Position, tokens: seq[A4Token]): A4Element
func a4parseElementNotInfix(fallbackPos: A4Position, tokens: seq[A4Token]): A4Element

# And it begins!

func a4parseBlock*(tokens: seq[A4Token]): seq[A4Element] =
  ## This is the root of the parser.
  ## Use this to parse A4 code.
  return a4parseBlockCore(tokens, ";")

func a4parseBlockCore(tokens: seq[A4Token], split: string): seq[A4Element] =
  # Something to note:
  # The reason why , is used rather than ; is to help with error detection.
  for stmt in a4parseGetStatements(tokens, split):
    if len(stmt) != 0:
      result.add(a4parseElement(stmt[0].position, stmt))

func a4parseElement(fallbackPos: A4Position, tokens: seq[A4Token]): A4Element =
  # this check is kind of obviously required,
  #  and is also relied upon for generating the fallbackPos for noInfix,
  #  even though really there's no good way it could propagate
  if len(tokens) == 0:
    return A4Element(
      position: fallbackPos,
      kind: a4eError,
      name: "Empty expression."
    )
  elif len(tokens) == 1:
    # If it's just a single token, it's an atom, unless it's structural.
    if tokens[0].depth() == 0:
      return A4Element(
        position: tokens[0].position,
        kind: a4eAtom,
        name: tokens[0].text
      )
    else:
      return A4Element(
        position: tokens[0].position,
        kind: a4eError,
        name: "Structural element as atom."
      )

  # Details of how infix operators work in A4/B, version 2:
  # Infix operators may still start just about anywhere.
  # However, starting them is NOT done through fallback processing,
  #  because that fallback processing system has turned out to be AWFUL.
  # JUST AWFUL.
  # Instead, there's precedence classes (innermost-to-outermost, like BODMAS)
  #  defined in lex.nim

  var bestSplitPrecedence = 0
  var bestSplit = -1
  var depth = 0
  for k, v in tokens.pairs():
    let modDepth = v.depth()
    depth += modDepth
    if depth != 0 or modDepth != 0:
      continue
    if k < len(tokens) - 1:
      # note that "X + {}" is completely valid from a parser perspective,
      #  so DON'T change this to use .depth
      #  also, making this avoid "(" was entirely wrong,
      #  calls are basically unary-style and that works out for the best here
      if tokens[k + 1].text == "[":
        # this is a complex
        continue
    var tknP = v.precedence
    if v.unary:
      if k == len(tokens) - 1:
        # There are no postfix unary operators in A4/B.
        continue
      if k != 0:
        # This is an unary operator being used in an un-unary way,
        #  which means that it's precedence "doesn't count"
        tknP = a4CustomTokenPrecedence
    elif k == 0 or k == len(tokens) - 1:
      # This hasn't got an excuse to be unary and it's being used in an unary way.
      continue
    if bestSplit == -1 or tknP > bestSplitPrecedence:
      bestSplit = k
      bestSplitPrecedence = tknP

  if bestSplit == -1:
    # no infix.
    return a4parseElementNotInfix(tokens[0].position, tokens)

  let here = tokens[bestSplit].position

  if tokens[bestSplit].unary and bestSplit == 0:
    # Unary operator, being used in an unary manner.
    # You know what must be done.
    let elementB = a4parseElement(here, tokens[bestSplit + 1..len(tokens) - 1])
    return A4Element(
      position: tokens[bestSplit].position,
      kind: a4eCall,
      name: tokens[bestSplit].text,
      elements: @[elementB]
    )

  # The token is valid for infix splitting. Split and parse.
  # NOTE: The recursive 'side' determines associativity.
  # The theoretical result of 1 - 1 - 1 - 1 can be either -2 or 0.
  # As things are, the result would be (((1) - 1) - 1) - 1 = -2.
  let elementA = a4parseElement(here, tokens[0..bestSplit - 1])
  let elementB = a4parseElement(here, tokens[bestSplit + 1..len(tokens) - 1])
  return A4Element(
    position: here,
    kind: a4eCall,
    name: tokens[bestSplit].text,
    elements: @[elementA, elementB]
  )

func a4parseElementNotInfix(fallbackPos: A4Position, tokens: seq[A4Token]): A4Element =
  if len(tokens) == 0:
    return A4Element(
      kind: a4eError,
      name: "Empty expression (as NotInfix; is this part of a complex address source?)"
    )

  var suggestion = ""

  if tokens[len(tokens) - 1].text == ")" and len(tokens) >= 2:
    # Calls and precedence modifiers are thankfully easy.
    # There's a hard rule in place that a call can only be preceded by a block name.
    if tokens[0].text == "(":
      # Precedence modifier.
      let plan = tokens[1..len(tokens) - 2]
      if not a4parseHasInterruption(plan):
        # A precedence modifier only exists to keep the infix splitter away, it has no real effect
        return a4parseElement(tokens[0].position, plan)
      suggestion = " (Mismatched '()' in precedence modifier?)"
    elif tokens[1].text == "(":
      # Call.
      let plan = tokens[2..len(tokens) - 2]
      if not a4parseHasInterruption(plan):
        return A4Element(
          position: tokens[0].position,
          kind: a4eCall,
          name: tokens[0].text,
          elements: a4parseBlockCore(plan, ",")
        )
      suggestion = " (Mismatched '()' in call?)"
  elif tokens[len(tokens) - 1].text == "]":
    # Complexes are the last part remaining. And also the most annoying.
    # See, calls are bound by a code of honour known as the "20kdc is very lazy" Carta.
    # NOT complexes.
    # Complexes can start at nearly any position.
    # They're also *sometimes* allowed to be left-recursive.
    # To be more precise, they're allowed to be left-recursive if infix operations aren't involved.
    # In the old parser, this was enforced here,
    #  because otherwise a = b[c]; parses as (a = b)[c].
    # The new parser considers infix & unary operators as having infinite precedence over
    #  absolutely everything else.
    # That said, generalizing precedence might be an option if I can make this work.
    for i in 1..len(tokens) - 2:
      if tokens[i].text == "[":
        let plan = tokens[i + 1..len(tokens) - 2]
        if a4parseHasInterruption(plan):
          continue
        # removing NotInfix necessary due to needing to make the 1-token
        #  handling more important than other handling to prevent massive
        #  syntax errors in parser refactor
        let baseElem = a4parseElement(tokens[i].position, tokens[0..i - 1])
        if baseElem.kind == a4eError:
          continue
        return A4Element(
          position: tokens[0].position,
          kind: a4eCall,
          name: "!complex",
          elements: @[baseElem] & a4parseBlockCore(plan, ",")
        )
    suggestion = " (Mismatched '[]' in indirection?)"
  elif tokens[0].text == "{" and tokens[len(tokens) - 1].text == "}":
    # This is a closure.
    let plan = tokens[1..len(tokens) - 2]
    if not a4parseHasInterruption(plan):
      return A4Element(
        position: tokens[0].position,
        kind: a4eClosure,
        elements: a4parseBlock(plan)
      )
    suggestion = " (Mismatched '{}' in closure?)"

  var helpText = ""
  for v in tokens:
    helpText &= v.text
  return A4Element(
    position: tokens[0].position,
    kind: a4eError,
    name: "Syntax error near '" & helpText & "'" & suggestion
  )
