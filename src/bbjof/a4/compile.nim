import ./lex
import ./parse
import ./values
import ../../bbjof
import ../mvsym
import ../merge
import ../prims
import ../parseint
import tables
import sugar
import strutils

type
  A4Result* = object
    errors*: seq[A4Element]
    # Only valid if len(errors) == 0.
    value*: A4Value

proc unwrap*(t: A4Result, v: var A4Result): A4Value =
  v.errors &= t.errors
  result = t.value

type
  # @0 is reappropriated as return value ONLY if not a statement.
  A4BuiltinCallInfo* = object
    pos*: A4Position
    args*: seq[A4Value]
    # isThisAStatement specifically and only matters for !call.
    isThisAStatement*: bool

proc err*(ci: A4BuiltinCallInfo, text: string): A4Element =
  result = A4Element(
    position: ci.pos,
    kind: a4eError,
    name: text
  )

type
  A4CompilerBuiltin* = ref object
    params*: string
    help*: string
    run*: (bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo) -> A4Result
  A4Compiler* = ref object
    # This controls a few things, but in particular,
    #  it controls whether the compiler actually does any work.
    # With this off, the compiler will avoid doing anything which
    #  could theoretically cause an error dependent on file content.
    # So disabling this allows you to feed the compiler blank files.
    real*: bool
    # This is the file cache. It's exposed to allow it to be indexed.
    objCache*: TableRef[string, BBJOF]
    # Gets a BBJOF object (or null on failure)
    objGetter: (string) -> BBJOF
    # This is the set of compiler builtins.
    builtins*: Table[string, A4CompilerBuiltin]
    uniqueLabelCounter: int
  A4BlockCompileState* = object
    # The compiler.
    compiler*: A4Compiler
    # The object as built so far.
    obj*: BBJOF
    # Object parents from innermost to outermost, including the current object
    # (this is used for constant resolution)
    objStack*: seq[BBJOF]
    # The mapping to perform on the object.
    alias*: TableRef[string, BBJOFMvsymTarget]
    # The "hanging return" - the unplaced execution label.
    # This defaults to "@call".
    # Whatever this is at the end is then auto-aliased to "@return".
    hangingReturn*: string

proc newA4Compiler*(objGetter: (string) -> BBJOF): A4Compiler =
  result = A4Compiler(real: true, objGetter: objGetter, objCache: newTable[string, BBJOF]())

func dummyObjGetter(name: string): BBJOF =
  result = BBJOF()

proc newA4Checker*(): A4Compiler =
  result = A4Compiler(objGetter: dummyObjGetter, objCache: newTable[string, BBJOF]())

proc getObject*(c: A4Compiler, name: string): BBJOF =
  ## Gets an object.
  ## Unless you have a good reason (dep scanning),
  ##  use this, not objCache or
  ##  objGetter.
  if c.objCache.hasKey(name):
    result = c.objCache[name]
  else:
    let file = c.objGetter(name)
    c.objCache[name] = file
    result = file

proc uniqueLabel*(c: A4Compiler, purpose: string): string =
  c.uniqueLabelCounter += 1
  result = " A4 " & purpose & " " & $c.uniqueLabelCounter

proc getIndInt*(bcs: var A4BlockCompileState, t: A4Value, val: var BBJOFInt): bool =
  ## Gets an indirect constant.
  ## ONLY USE IN COMPILER BUILT-INS, IT'S NOT STRICTLY-SPEAKING, AH, "SCOPING-COMPLIANT"
  if not t.isKind(a4vLabel):
    return false
  # Ok, so to make this work, we modify the value over and over.
  # That jusifies a copy into something that can be kept on-stack.
  var tgt = BBJOFMvsymTarget(name: t.label, offset: t.offset)
  var currentObjIdx = 0
  for i in 0..255:
    if tgt.name != "0":
      # Either non-trivally constant or an error, so the slow path is fine
      var foundIn: BBJOF
      var idx: int
      while true:
        foundIn = bcs.objStack[currentObjIdx]
        idx = foundIn.findExport(tgt.name)
        if idx != -1:
          break
        # Not here.
        # Go up the stack...
        currentObjIdx += 1
        if currentObjIdx == len(bcs.objStack):
          # No way to go up further!
          return false
      # So it is here!
      var impIdx = foundIn.exports[idx].target.importIdx
      if impIdx < 0:
        # Allocation. This MAY be updated later, but for now rules are rules.
        return false
      else:
        # Import. Oh, what fun.
        tgt.name = foundIn.imports[impIdx]
        tgt.offset += foundIn.exports[idx].target.offset
    else:
      # Trivally constant.
      val = tgt.offset
      return true
  raise newException(CatchableError, "Why did you think putting a bunch of symbols into a loop and then using that as a constant was a good idea? (" & t.label & " - gave up at " & tgt.name & ")")

# Early declaration used in compileElement.
# Note that this is the internal version. compileElement uses it for closures, so it has to be.
proc compile(com: A4Compiler, label: string, blk: seq[A4Element], parObjStack: seq[BBJOF]): A4Result

proc compileElement*(bcs: var A4BlockCompileState, element: A4Element, isThisAStatement: bool): A4Result =
  ## Compiles a single element.
  if element.kind == a4eAtom:
    try:
      let ofs = parseBBJOFInt(element.name)
      result.value = A4Value(
        kind: a4vLabel,
        label: "0",
        offset: ofs
      )
    except ValueError:
      result.value = A4Value(
        kind: a4vLabel,
        label: element.name,
        offset: 0
      )
  elif element.kind == a4eCall:
    var name = element.name
    if name == ":":
      name = "equ"
    var args = element.elements
    if (not bcs.compiler.builtins.contains(element.name)) and (len(element.name) == 0 or element.name[0] != '!'):
      # If it's not "must be builtin"-syntax,
      #  and it's an unknown name...
      # Syntactic sugar: A(B) = !call(!importBJO(A), B)
      args = @[A4Element(
        position: element.position,
        kind: a4eCall,
        name: "!importBJO",
        elements: @[
          A4Element(
            position: element.position,
            kind: a4eAtom,
            name: name
          )
        ]
      )] & args
      name = "!call"
    # Continuing on.
    let builtin = bcs.compiler.builtins.getOrDefault(name, nil)
    var argsValues: seq[A4Value]
    for v in args:
      argsValues.add(compileElement(bcs, v, false).unwrap(result))
    if builtin == nil:
      result.errors.add(
        A4Element(
          position: element.position,
          kind: a4eError,
          name: "No such compiler builtin: " & name
        )
      )
    elif len(result.errors) == 0:
      # Note that this only executes if no errors have already come up -
      #  don't even try executing if the result will be thrown away anyway
      #  and the inputs are potentially broken
      result.value = builtin.run(bcs, A4BuiltinCallInfo(
        pos: element.position,
        args: argsValues,
        isThisAStatement: isThisAStatement
      )).unwrap(result)
  elif element.kind == a4eClosure:
    result = bcs.compiler.compile("(closure " & $element.position & ")", element.elements, bcs.objStack)
  elif element.kind == a4eError:
    result.errors.add(element)
  else:
    result.errors.add(A4Element(
      position: element.position,
      kind: a4eError,
      name: "unable to compile " & $element.kind & ": whoops!"
    ))

proc compile(com: A4Compiler, label: string, blk: seq[A4Element], parObjStack: seq[BBJOF]): A4Result =
  ## Returns an A4Value of kind a4vBlock.
  ## Note that compilation errors inherit parsing errors.
  var bcs = A4BlockCompileState(
    compiler: com,
    obj: BBJOF(),
    alias: newTable[string, BBJOFMvsymTarget](),
    hangingReturn: "@call"
  )
  bcs.objStack = @[bcs.obj] & parObjStack
  result.value = A4Value(
    kind: a4vObj,
    obj: bcs.obj,
    label: label
  )
  var lastPos: A4Position
  for elm in blk:
    lastPos = elm.position
    try:
      let val = bcs.compileElement(elm, true).unwrap(result)
      if val.isKind(a4vLabel):
        if val.offset != 0:
          result.errors.add(A4Element(
            position: elm.position,
            kind: a4eError,
            # it COULD be done but it's a stupid idea
            name: "Attempted to place label in hanging return flow with an offset"
          ))
        else:
          bcs.obj.merge(newBBJOFPrimPipe(bcs.hangingReturn, val.label), $elm.position)
    except CatchableError as e:
      result.errors.add(A4Element(
        position: lastPos,
        kind: a4eError,
        name: e.msg
      ))
  # The final call/return setup steps:
  # Unless there's an override, auto-alias @call to itself (to make it export)
  if not bcs.alias.hasKey("@call"):
    bcs.alias["@call"] = BBJOFMvsymTarget(name: "@call", offset: 0)
  # Attach hanging return to actual return
  bcs.alias[bcs.hangingReturn] = BBJOFMvsymTarget(name: "@return", offset: 0)
  try:
    bcs.obj.link()
    bcs.obj.mvsym(bcs.alias, bbjofmvmCleanupExports)
    # Final-final-final-final ultimate fallback:
    # If there is no export for @call, add a pipe
    # This will either prevent empty closures from blowing up or it'll get optimized away
    if not bcs.obj.hasExport("@call"):
      bcs.obj.merge(newBBJOFPrimPipe("@return", "@call"), "ultimateFallbackLink")
    bcs.obj.link()
  except CatchableError as e:
    var res = ""
    for k, v in bcs.alias.pairs():
      res &= "\n " & k & " -> " & $v
    result.errors.add(A4Element(
      position: lastPos,
      kind: a4eError,
      name: "Alias map: " & e.msg & res
    ))

proc compile*(com: A4Compiler, label: string, blk: seq[A4Element]): A4Result =
  ## Root compile function without the complex stuff.
  return compile(com, label, blk, @[])

