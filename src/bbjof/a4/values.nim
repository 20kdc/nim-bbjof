import ../../bbjof
import ../a4core
import ../mvsym
import sugar

type
  A4ValueKind* = enum
    # NOTE: These are the internal types.
    # They all have particular behaviors that make them work nicely,
    #  but which also make them painful as hell.
    # Also, null exists.
    # Any number or label.
    a4vLabel,
    # An object. The callee may choose to instance this object. ('closure')
    # Has a label attached for debug reasons.
    a4vObj,
    # base[suffix...] // implicitly indirect
    a4vComplex,
    # base $ offset
    a4vExtract,
    # base ^ label
    a4vAttachment
  A4Value* = ref object
    kind*: A4ValueKind
    obj*: BBJOF # obj
    label*: string # label, attachment, obj (for debug reasons)
    offset*: BBJOFInt # label, extract
    base*: A4Value # indirect, extract, attachment
    suffix*: seq[A4Value] # complex

proc isKind*(t: A4Value, kind: A4ValueKind): bool =
  t != nil and t.kind == kind

proc isSource*(t: A4Value): bool =
  (t != nil) and
  (t.kind == a4vLabel or
  t.kind == a4vComplex or
  t.kind == a4vExtract or
  t.kind == a4vAttachment)

proc getInt*(t: A4Value, val: var BBJOFInt): bool =
  ## Gets a non-indirect constant.
  ## Note that you MAY be looking for A4BlockCompileState.getIndInt instead.
  if not t.isKind(a4VLabel):
    return false
  if t.label != "0":
    return false
  val = t.offset
  return true

type
  # This is really compile/copy's jurisdiction
  # maybe move this into a separate file, or move it to BBJOF-level like A4 Core, IDK
  A4Copy* = object
    source*: A4CoreAdjustment
    target*: A4CoreAdjustment
  # These are what we're here for
  A4ValueToAdjustmentResult* = object
    errors*: seq[string]
    preparation*: seq[A4Copy]
    result*: A4CoreAdjustment
  A4ValueToSourceResult* = object
    errors*: seq[string]
    preparation*: seq[A4Copy]
    result*: A4CoreSource

proc toAdjustment*(val: A4CoreSource): A4CoreAdjustment =
  A4CoreAdjustment(
    kind: a4caAddress,
    source: @[val]
  )

proc indirect*(val: A4CoreAdjustment, labelmaker: () -> string): A4ValueToSourceResult =
  ## Setup a copy from an adjustment to a temporary value.
  let temp = labelmaker()
  result.preparation.add(A4Copy(source: val, target: BBJOFMvsymTarget(name: temp).toA4CoreAdjustment))
  # A temporary value stored inside the instruction, defaulting to 0.
  result.result = A4CoreSource(label: "0", attachments: @[temp])

proc indirect*(val: A4ValueToAdjustmentResult, labelmaker: () -> string): A4ValueToSourceResult =
  ## Setup a copy from an adjustment to a temporary value.
  result.errors = val.errors
  result.preparation = val.preparation
  let res = val.result.indirect(labelmaker)
  result.errors &= res.errors
  result.preparation &= res.preparation
  result.result = res.result

# -- Just a reminder... --
# An Adjustment is an address.
# If someone requests an Adjustment, they're requesting an address.
# So don't indirect an address for no reason.
# But if someone requests a Source, what they really want is a byte,
#  so if you only have an Adjustment, they want that in indirect form.
# --

proc toAdjustment*(val: A4Value, labelmaker: () -> string): A4ValueToAdjustmentResult

proc toSource*(val: A4Value, labelmaker: () -> string): A4ValueToSourceResult =
  ## Converts to an A4CoreSource, with an added structure containing preparation steps (for indirection).
  if val == nil:
    result.errors = @["Can't convert nil to source"]
  elif val.kind == a4vExtract:
    # Extract
    let res = val.base.toAdjustment(labelmaker)
    result.errors = res.errors
    result.preparation = res.preparation
    if len(result.errors) != 0:
      # If there are errors, stop immediately since this can otherwise lead to index-out-ofbounds
      return
    elif res.result.kind != a4caAddress:
      # This adds a further safety check
      result.errors.add("Can't extract a byte from a non-address")
    else:
      # Alright, which source is being extracted
      # Get a valid source - base if outside the suffix.
      if val.offset >= len(res.result.source):
        result.result = res.result.source[len(res.result.source) - 1]
      else:
        result.result = res.result.source[val.offset]
      # In either case, if there's already an extraction, it applies,
      #  otherwise override
      if result.result.extract == -1:
        result.result.extract = val.offset
  elif val.kind == a4vAttachment:
    result = val.base.toSource(labelmaker)
    result.result.attachments.add(val.label)
  else:
    result = val.toAdjustment(labelmaker).indirect(labelmaker)

proc toAdjustment*(val: A4Value, labelmaker: () -> string): A4ValueToAdjustmentResult =
  if val == nil:
    result.errors = @["Can't convert nil to adjustment"]
  elif val.kind == a4vLabel:
    result.result = BBJOFMvsymTarget(name: val.label, offset: val.offset).toA4CoreSource.toAdjustment
  elif val.kind == a4vObj:
    result.result = A4CoreAdjustment(
      kind: a4caClosure,
      closure: val.obj
    )
  elif val.kind == a4vComplex:
    if val.base.kind != a4vLabel:
      # the current preparation infrastructure can fail badly with operations that get erased
      # theoretically, the correct solution is to track preparations per-source.
      # practically, that's not reasonable.
      result.errors.add("Complex base was not address")
    else:
      # This sets up the base.
      result = val.base.toAdjustment(labelmaker)
      # There are two problems here:
      # 1. The suffix is in reverse order from the order BBJOF uses.
      # 2. We need to add things to the start.
      for part in val.suffix:
        let entry = part.toSource(labelmaker)
        result.errors &= entry.errors
        result.preparation &= entry.preparation
        # This solves both problems.
        result.result.source.insert(entry.result, 0)
  elif val.kind == a4vExtract:
    # This indicates someone wants an immediate value outside of a 'proper' immediate context.
    # IF this is possible, do it, but if it's not possible give an error
    var constVal: BBJOFInt
    if val.base.getInt(constVal):
      # This is easy, so do things the easy way
      var res = (constVal shr (val.offset * 8)) and 0xFF
      result.result = BBJOFMvsymTarget(name: "id." & $res, offset: 0).toA4CoreSource.toAdjustment
    elif not val.base.isKind(a4vLabel):
      # Has to be label to handle as a relocation
      result.errors = @["Cannot convert anything except a constant or label to an identity-table access."]
    else:
      # A relocation is involved, so use page_dec[N+1]
      var ofs = 1 shl (val.offset * 8)
      result.result = A4CoreAdjustment(
        kind: a4caAddress,
        source: @[
          A4CoreSource(label: val.base.label, offset: val.base.offset + ofs, extract: val.offset),
          A4CoreSource(label: "page_dec", offset: 0, extract: -1)
        ]
      )
  else:
    result.errors = @["Can't handle " & $val.kind & " as an adjustment"]
