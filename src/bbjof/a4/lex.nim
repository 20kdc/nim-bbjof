import npeg
# Ew!
import npeg/capture

type
  A4Position* = object
    # This is for the human-readable variant only.
    source*: string
    ## NOTE: For A4LS reasons, this should ideally use the LSP definition of "line" and "character".
    line*: int
    character*: int
  A4Token* = object
    position*: A4Position
    text*: string

func `$`*(pos: A4Position): string =
  pos.source & "(" & $(pos.line + 1) & ", " & $(pos.character + 1) & ")"

func depthClosure*(token: A4Token): int =
  ## Depth only in regards to closures.
  ## This is used by semicolon splitting,
  ##  to isolate it from other errors
  if token.text == "{":
    return 1
  elif token.text == "}":
    return -1
  return 0

func depth*(token: A4Token): int =
  ## Depth in general
  if token.text == "(":
    return 1
  elif token.text == ")":
    return -1
  elif token.text == "[":
    return 1
  elif token.text == "]":
    return -1
  return token.depthClosure()

func unary*(token: A4Token): bool =
  return token.text == "%"

const a4CustomTokenPrecedence* = -1

# Precedence.
# This is in the same "innermost to outermost" order
#  as the common mnemonic BODMAS.
func precedence*(token: A4Token): int =
  if token.text == "+":
    return 0
  elif token.text == "$":
    return 1
  elif token.text == "%":
    return 2
  elif token.text == "^":
    return 3
  elif token.text == "=":
    return 4
  elif token.text == ":":
    return 5
  else:
    return a4CustomTokenPrecedence

# MAKE SURE TO KEEP IN SYNC WITH man/a4.5
# (and ideally the highlighting rules, inaccurate as they are)
let a4lexer* = peg "FILE":
  NEWLINE <- "\r\n" | '\r' | '\n'
  COMMENT_LINE <- "//" * *(1 - NEWLINE) * NEWLINE
  COMMENT_BLOCK <- "/*" * ((*(1 - "*/") * "*/") | E"end of block comment (*/)")
  WSC <- Cntrl | ' ' | COMMENT_LINE | COMMENT_BLOCK
  WS <- +WSC
  # note: '!' is the compiler-reserved prefix. not an operator.
  # note: '/' must not be registered, ever - paths are kept singular tokens to simplify block call parsing
  OPERATOR_BLOCK <- '(' | ')' | '{' | '}' | '[' | ']' | ';' | ','
  OPERATOR_OTHER <-  '+' | '^' | '%' | ':' | '$' | '='
  OPERATOR <- OPERATOR_BLOCK | OPERATOR_OTHER
  # note that this captures the interior
  STRING <- '"' * ((>(*(1 - '"')) * '"') | E"end of string")
  # same here
  BLOCK_STRING <- '<' * R(term, *(1 - '>')) * '>' * ((>(*(1 - ("</" * R(term) * '>'))) * "</" * R(term) * '>') | E"end of block string")
  ID <- +(1 - (WSC | OPERATOR | '"'))
  TOKEN <- WS | BLOCK_STRING | >ID | >OPERATOR | STRING
  FILE <- *TOKEN * !1

func a4ConvertPosition*(source: string, sourceName: string, pos: int): A4Position =
  result = A4Position(source: sourceName)
  for i in 0 .. pos - 1:
    if source[i] == '\n':
      result.line += 1
      result.character = 0
    else:
      result.character += 1

# In case you're wondering: There's no good "get capture position" API
#  in NPEG. If NPEG updates and breaks things, you should use mr.captures
#  and set all of the positions to something funny.
func a4ConvertTokens*(source: string, sourceName: string, mr: MatchResult[char]): seq[A4Token] =
  var posI = 0
  var pos = A4Position(source: sourceName)
  for v in mr.cs.collectCaptures():
    while posI < v.si:
      if source[posI] == '\n':
        pos.line += 1
        pos.character = 0
      else:
        pos.character += 1
      posI += 1
    result.add(A4Token(
      position: pos,
      text: v.s
    ))

