import ../values
import ../lex # for $ on pos
import ../compile
import ../../mvsym
import ../../merge
import ../../a4core
import tables
import sugar

proc a4biCopyInternal*(bcs: var A4BlockCompileState, pos: string, copy: A4Copy): void =
  ## Writes a copy from some source to some target.
  var inst = deepCopy(bcs.compiler.getObject("instruction"))
  var stmt = newTable[string, A4CoreAdjustment]()
  stmt["@call"] = BBJOFMvsymTarget(name: bcs.hangingReturn).toA4CoreAdjustment
  bcs.hangingReturn = bcs.compiler.uniqueLabel("Internal Hanging Return")
  stmt["@0"] = copy.source
  stmt["@1"] = copy.target
  stmt["@2"] = BBJOFMvsymTarget(name: bcs.hangingReturn).toA4CoreAdjustment
  #echo $copy.source & " " & $copy.target & " " & bcs.hangingReturn
  inst.applyA4Core(stmt)
  inst.link()
  bcs.obj.merge(inst, pos)

proc a4biCopy*(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 2:
    result.errors.add(ci.err("= takes two parameters (dst, src)"))
  elif not ci.args[0].isSource():
    result.errors.add(ci.err("source must be a source"))
  elif not ci.args[1].isSource():
    result.errors.add(ci.err("destination must be a destination"))
  else:
    let posStr = $ci.pos
    let bcsCompiler = bcs.compiler
    let copy = () => bcsCompiler.uniqueLabel("Intermediate")

    let a1 = ci.args[1].toAdjustment(copy)
    for v in a1.errors:
      result.errors.add(ci.err(v))
    for v in a1.preparation:
      a4biCopyInternal(bcs, posStr, v)

    let a0 = ci.args[0].toAdjustment(copy)
    for v in a0.errors:
      result.errors.add(ci.err(v))
    for v in a0.preparation:
      a4biCopyInternal(bcs, posStr, v)

    a4biCopyInternal(bcs, posStr, A4Copy(
      source: a1.result,
      target: a0.result
    ))
