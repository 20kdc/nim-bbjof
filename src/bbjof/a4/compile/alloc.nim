import ../compile
import ../values
import ../../../bbjof
import tables

proc btAlloc(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 4:
    result.errors.add(ci.err("!alloc has four params (size, alignStart, alignStep, init)"))
  elif (not ci.args[0].isKind(a4vLabel)) or ci.args[0].label != "0" or ci.args[0].offset < 0:
    result.errors.add(ci.err("size must be a valid size"))
  elif (not ci.args[1].isKind(a4vLabel)) or ci.args[1].label != "0":
    result.errors.add(ci.err("alignStart must be a valid alignment start"))
  elif (not ci.args[2].isKind(a4vLabel)) or ci.args[2].label != "0" or ci.args[2].offset < 0:
    result.errors.add(ci.err("alignStep must be a valid alignment step"))
  elif (not ci.args[3].isKind(a4vLabel)) or ci.args[3].label != "0":
    result.errors.add(ci.err("init must be a boolean (0 or 1)"))
  else:
    let allocName = bcs.compiler.uniqueLabel("Allocation")
    if ci.args[3].offset != 0:
      if bcs.compiler.real:
        # this operation can eat memory if written incorrectly
        # so don't let it be executed literally half-way through typing
        var data = newString(ci.args[0].offset)
        for i in 0 .. len(data) - 1:
          data[i] = (char) 0
        let allocIdx = len(bcs.obj.allocs)
        bcs.obj.allocs.add(BBJOFAlloc(
          size: len(data),
          alignStart: ci.args[1].offset,
          alignStep: ci.args[2].offset,
          data: data
        ))
        bcs.obj.exports.add(BBJOFExp(
          name: allocName,
          target: BBJOFRef(index: allocIdx)
        ))
    else:
      let allocIdx = len(bcs.obj.allocs)
      bcs.obj.allocs.add(BBJOFAlloc(
        size: ci.args[0].offset,
        alignStart: ci.args[1].offset,
        alignStep: ci.args[2].offset
      ))
      bcs.obj.exports.add(BBJOFExp(
        name: allocName,
        target: BBJOFRef(index: allocIdx)
      ))
    result.value = A4Value(
      kind: a4vLabel,
      label: allocName,
      offset: 0
    )

proc btAttachReloc(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 3:
    result.errors.add(ci.err("!attachReloc has three params (source, addrByte, target)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("source must be a label"))
  elif (not ci.args[1].isKind(a4vLabel)) or ci.args[1].label != "0" or ci.args[1].offset < 0:
    result.errors.add(ci.err("addrByte must be a valid byte index"))
  elif not ci.args[2].isKind(a4vLabel):
    result.errors.add(ci.err("target must be a label"))
  else:
    if bcs.compiler.real:
      # this operation is similar to the "pipe" prim in that it synthesizes imports
      # ...which can cause alias mapping failures if the required symbols aren't present,
      #  due to, say, the general semantics of bcs.compiler.real = false
      #  so be sure we're doing this for real
      let base = len(bcs.obj.imports)
      bcs.obj.imports.add(ci.args[0].label)
      bcs.obj.imports.add(ci.args[2].label)
      bcs.obj.relocs.add(BBJOFRel(
        source: BBJOFRef(index: -(base + 1), offset: ci.args[0].offset),
        addrByte: ci.args[1].offset,
        target: BBJOFRef(index: -(base + 2), offset: ci.args[2].offset)
      ))

proc btPutText(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 2:
    result.errors.add(ci.err("!putText has two params (target, text)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("target must be a label"))
  elif not ci.args[1].isKind(a4vLabel) or ci.args[1].offset != 0:
    result.errors.add(ci.err("text must be a label with no additives"))
  else:
    if bcs.compiler.real:
      # this operation is similar to the "pipe" prim in that it synthesizes imports
      let base = len(bcs.obj.imports)
      bcs.obj.imports.add(ci.args[0].label)
      bcs.obj.imports.add("0")
      var idx: BBJOFInt = 0
      for chr in ci.args[1].label:
        bcs.obj.relocs.add(BBJOFRel(
          source: BBJOFRef(index: -(base + 2), offset: (BBJOFInt) (uint8) chr),
          addrByte: 0,
          target: BBJOFRef(index: -(base + 1), offset: ci.args[0].offset + idx)
        ))
        idx += 1

proc addAllocBuiltins*(compiler: A4Compiler): void =
  compiler.builtins.add("!alloc", A4CompilerBuiltin(
    params: "(size: int, alignStart: int, alignStep: int, init: int): label",
    help: "Allocates some memory, which can be initialized (init != 0). A label to it is returned.",
    run: btAlloc
  ))
  compiler.builtins.add("!attachReloc", A4CompilerBuiltin(
    params: "(source: label, addrByte: int, target: label): void",
    help: "Attaches a relocation.",
    run: btAttachReloc
  ))
  compiler.builtins.add("!putText", A4CompilerBuiltin(
    params: "(target: label, text: label): label",
    help: "A quick way to put some text into the file.",
    run: btPutText
  ))
