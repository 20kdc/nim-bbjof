import ../lex # needed for correct position information sometimes
import ../compile
import ../values
import ../../mvsym
import ../../../bbjof
import tables
import strutils
import ./sym
import ./conv
import ./alloc
import ./call

proc debugDumpValue(val: A4Value): string =
  if val == nil:
    return "void"
  elif val.kind == a4vAttachment:
    return "^(" & debugDumpValue(val.base) & ", " & $val.label & ")"
  elif val.kind == a4vComplex:
    var complexDetails = "!complex("
    complexDetails &= debugDumpValue(val.base)
    for v in val.suffix:
      complexDetails &= ", "
      complexDetails &= debugDumpValue(v)
    complexDetails &= ")"
    return complexDetails
  elif val.kind == a4vExtract:
    return "$(" & debugDumpValue(val.base) & ", " & $val.offset & ")"
  elif val.kind == a4vLabel:
    return "label: " & val.label & "+" & $val.offset
  elif val.kind == a4vObj:
    return "closure: " & val.label
  else:
    return "unhandled: " & $val.kind

proc btDebug(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 1:
    result.errors.add(ci.err("!debug expects 1 argument (value)"))
  else:
    result.errors.add(ci.err(debugDumpValue(ci.args[0])))

proc btRepeat(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  var repTimes: BBJOFInt = 0
  if len(ci.args) < 2:
    result.errors.add(ci.err("!repeat expects at least 2 arguments (times, code)"))
  elif (not bcs.getIndInt(ci.args[0], repTimes)) or repTimes < 0:
    result.errors.add(ci.err("!repeat expects an amount of times that is a constant number above or equal to 0"))
  elif not ci.args[len(ci.args) - 1].isKind(a4vObj):
    result.errors.add(ci.err("!repeat expects a closure"))
  elif (len(ci.args) mod 2) != 0:
    result.errors.add(ci.err("!repeat expects base/multiplier pairs"))
  else:
    var muls: seq[tuple[name: string, base: string, baseOffset: BBJOFInt, mul: BBJOFInt]]
    var failed: bool = false
    for j in 0 .. ((len(ci.args) shr 1) - 2):
      let baseArgIdx = (j * 2) + 1
      var mul: BBJOFInt
      if (not ci.args[baseArgIdx].isKind(a4vLabel)) or (not bcs.getIndInt(ci.args[baseArgIdx + 1], mul)):
        result.errors.add(ci.err("invalid base/multiplier pair"))
        failed = true
      muls.add((name: "@" & $j, base: ci.args[baseArgIdx].label, baseOffset: ci.args[baseArgIdx].offset, mul: mul))
    if (not failed) and bcs.compiler.real:
      # Only actually run the repetition for a real compile
      let subObj = ci.args[len(ci.args) - 1].obj

      type
        IdxReplacement = object
          base: string
          lhs: string
          rhs: string
          ofs: int

      var replacements: seq[IdxReplacement]

      proc handleRI(res: var A4Result, s: string): void =
        let idx = s.find("!repeatIndex")
        if idx != -1:
          if idx + 15 > len(s):
            res.errors.add(ci.err("invalid repeat index expression"))
            return
          var ofs = parseHexInt(s[idx + 13..idx + 14])
          if s[idx + 12] == 'M':
            ofs = -ofs
          replacements.add(IdxReplacement(
            base: s,
            lhs: s[0..idx - 1],
            rhs: s[idx + 15..len(s) - 1],
            ofs: ofs
          ))

      for v in subObj.imports:
        handleRI(result, v)
      for v in subObj.exports:
        handleRI(result, v.name)

      for i in 0 .. repTimes - 1:
        var work = newTable[string, BBJOFMvsymTarget]()

        for v in replacements:
          work[v.base] = BBJOFMvsymTarget(name: v.lhs & $(i + v.ofs) & v.rhs)

        for mul in muls:
          work[mul.name] = BBJOFMvsymTarget(name: mul.base, offset: mul.baseOffset + (i * mul.mul))
        bcs.simpleCall(subObj, work, $ci.pos & "[" & $i & "]")

proc addAllBuiltins*(compiler: A4Compiler): void =
  # - general groups -
  compiler.addSymBuiltins()
  compiler.addConvBuiltins()
  compiler.addAllocBuiltins()
  # - things actually defined here (basically, welcome to case-specific-code-land) -
  compiler.builtins.add("!debug", A4CompilerBuiltin(
    params: "(input: value): void",
    help: "Reports an error with full details of the debugged value.",
    run: btDebug
  ))
  compiler.builtins.add("!repeat", A4CompilerBuiltin(
    params: "(times: ind.int, (base: label, mul: ind.int)..., code: closure): void",
    help: "Repeats the same closure over and over again.\nEach parameter to the closure has a given base label and multiplier for each index.\nTo help communicate between iterations, the text '!repeatIndexM00' (0), '!repeatIndexM0A' (-10), etc. in a symbol is replaced.",
    run: btRepeat
  ))
