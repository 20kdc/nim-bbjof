import ../compile
import ../values
import ../../mvsym
import tables

proc btAlias(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 2:
    result.errors.add(ci.err("Expected 2 arguments to !alias (target, source)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("Expected !alias's target to be a label"))
  elif not ci.args[1].isKind(a4vLabel):
    result.errors.add(ci.err("Expected !alias's source to be a label"))
  else:
    if bcs.alias.hasKey(ci.args[1].label):
      result.errors.add(ci.err("!alias's source '" & ci.args[1].label & "' is already globalized"))
    else:
      bcs.alias.add(ci.args[1].label, BBJOFMvsymTarget(
        name: ci.args[0].label,
        offset: ci.args[0].offset - ci.args[1].offset
      ))

proc btGlobal(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 1:
    result.errors.add(ci.err("Expected 1 argument to !global (target)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("Expected !global's target to be a label"))
  else:
    if bcs.alias.hasKey(ci.args[0].label):
      result.errors.add(ci.err("!global's target '" & ci.args[0].label & "' is already globalized"))
    else:
      bcs.alias.add(ci.args[0].label, BBJOFMvsymTarget(
        name: ci.args[0].label,
        offset: ci.args[0].offset
      ))

proc addSymBuiltins*(compiler: A4Compiler): void =
  compiler.builtins.add("!alias", A4CompilerBuiltin(
    params: "(target: label, source: label): void",
    help: "Aliases source to target after the block is compiled - it's also made global. See !global.",
    run: btAlias
  ))
  compiler.builtins.add("!global", A4CompilerBuiltin(
    params: "(targets: label...): void",
    help: "Marks the targets as global by aliasing them to themselves; they won't be deleted.",
    run: btGlobal
  ))
