# This is the "outer core".
# It's the translator between the complex AST world and the simpler A4 Core world.

import ../lex # needed for $ on a position to not use the default dumper
import ../compile
import ../values
import ../../mvsym
import ../../merge
import ../../a4core
import ../../../bbjof
import ./copy
import tables
import sugar

# for debug (keep around) {
# import ../../write
# import streams
# }

proc addInfo(adj: TableRef[string, A4CoreAdjustment]): string =
  for k, v in adj.pairs():
    result &= "\n\t" & k & " = " & $v

proc simpleCall*(bcs: var A4BlockCompileState, subObj: BBJOF, work: TableRef[string, BBJOFMvsymTarget], why: string): void =
  ## This performs "simple calls".
  ## Basically, this is a less complicated way to perform a call,
  ##  useful for when the compiler needs to call other code.
  ## It is, however, less flexible.
  var workingCopy = deepCopy(subObj)
  if workingCopy.hasExport("@call"):
    work["@call"] = BBJOFMvsymTarget(name: bcs.hangingReturn)
    bcs.hangingReturn = bcs.compiler.uniqueLabel("Hanging Return (after " & why & ")")
  if workingCopy.imports.contains("@return"):
    work["@return"] = BBJOFMvsymTarget(name: bcs.hangingReturn)
  workingCopy.mvsym(work, bbjofmvmLocal)
  bcs.obj.merge(workingCopy, why)

proc a4biCall*(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  ## Almost, but not quite, the actual core of A4/B.
  # -- Arg Check --
  if len(ci.args) < 1:
    result.errors.add(ci.err("Expected at least the target closure for !call"))
    return result
  elif ci.args[0].kind != a4vObj:
    result.errors.add(ci.err("Expected !call's argument to be a closure"))
    return result
  # -- Prepare Params --
  var params: seq[A4Value]
  if not ci.isThisAStatement:
    result.value = A4Value(
      kind: a4vLabel,
      label: bcs.compiler.uniqueLabel("Call Return Value")
    )
    params.add(result.value)
  for i in 1 .. len(ci.args) - 1:
    params.add(ci.args[i])
  # -- Cleanup Unused Parameters --
  var workingCopy = deepCopy(ci.args[0].obj)
  var preWorkCleanupMap = newTable[string, BBJOFMvsymTarget]()
  preWorkCleanupMap["@call"] = BBJOFMvsymTarget(name: "@call")
  preWorkCleanupMap["@return"] = BBJOFMvsymTarget(name: "@return")
  for k, v in params.pairs():
    let name = "@" & $k
    preWorkCleanupMap[name] = BBJOFMvsymTarget(name: name)
  try:
    workingCopy.mvsym(preWorkCleanupMap, bbjofmvmLocal)
    workingCopy.link()
  except CatchableError as e:
    result.errors.add(ci.err("During PWC of '" & ci.args[0].label & "': " & e.msg & " (did you forget a parameter?)"))
    return result
  # -- Statement: Init --
  var work = newTable[string, A4CoreAdjustment]()
  # -- Statement: Parameters --
  for k, v in params.pairs():
    # have to capture bcsCompiler here
    let bcsCompiler = bcs.compiler
    let toAdj = v.toAdjustment(() => bcsCompiler.uniqueLabel("Intermediate"))
    for v in toAdj.preparation:
      a4biCopyInternal(bcs, $ci.pos, v)
    for v in toAdj.errors:
      result.errors.add(ci.err(v))
    if len(toAdj.errors) == 0:
      work["@" & $k] = toAdj.result
  # -- Statement: Handling of Call/Return --
  # note that the preparation steps perform their own call/return pipelining,
  # so this must occur here
  if workingCopy.hasExport("@call"):
    work["@call"] = BBJOFMvsymTarget(name: bcs.hangingReturn).toA4CoreAdjustment
    bcs.hangingReturn = bcs.compiler.uniqueLabel("Hanging Return")
  if workingCopy.imports.contains("@return"):
    work["@return"] = BBJOFMvsymTarget(name: bcs.hangingReturn).toA4CoreAdjustment
  # -- Finishing --
  try:
    workingCopy.applyA4Core(work)
    workingCopy.link()
  except CatchableError as e:
    result.errors.add(ci.err("During apply of '" & ci.args[0].label & "': " & e.msg & " " & addInfo(work)))
    return result
  try:
    bcs.obj.merge(workingCopy, $ci.pos)
  except CatchableError as e:
    result.errors.add(ci.err("During merge of '" & ci.args[0].label & "': " & e.msg & " " & addInfo(work)))
    return result
