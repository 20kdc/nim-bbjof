import ../compile
import ../values
import ./call
import ./copy
import ../../../bbjof
import tables

proc btExtract(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  var ofs: BBJOFInt
  if len(ci.args) != 2:
    result.errors.add(ci.err("Expected 2 arguments to $ (source, index)"))
  elif not ci.args[0].isSource():
    result.errors.add(ci.err("Expected $'s source to be a source"))
  elif (not bcs.getIndInt(ci.args[1], ofs)) or ofs < 0:
    result.errors.add(ci.err("Expected $'s index to be a valid byte index"))
  else:
    result.value = A4Value(
      kind: a4vExtract,
      base: ci.args[0],
      offset: ofs
    )

proc btZero(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 1:
    result.errors.add(ci.err("Expected 1 argument to % (target)"))
  elif not ci.args[0].isSource():
    result.errors.add(ci.err("Expected %'s source to be a source"))
  else:
    result.value = A4Value(
      kind: a4vExtract,
      base: ci.args[0],
      offset: 0
    )

proc btAttachment(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 2:
    result.errors.add(ci.err("Expected 2 arguments to ^ (source, label)"))
  elif not ci.args[0].isSource():
    result.errors.add(ci.err("Expected ^'s source to be a source"))
  elif (not ci.args[1].isKind(a4vLabel)) or ci.args[1].offset != 0:
    result.errors.add(ci.err("Expected ^'s label to be a valid label, and just a label"))
  else:
    result.value = A4Value(
      kind: a4vAttachment,
      base: ci.args[0],
      label: ci.args[1].label
    )

proc btComplex(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) < 1:
    result.errors.add(ci.err("Expected at least 1 argument to !complex (base, ...)"))
  else:
    for i in 0 .. len(ci.args) - 1:
      if not ci.args[i].isSource():
        result.errors.add(ci.err("All arguments to !complex must be sources (arg " & $i & " wasn't)"))
        break
    result.value = A4Value(
      kind: a4vComplex,
      base: ci.args[0],
      suffix: ci.args[1..len(ci.args) - 1]
    )

proc btAdd(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  var ofs: BBJOFInt = 0
  if len(ci.args) != 2:
    result.errors.add(ci.err("Expected 2 arguments to + (base, mod)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("the first argument to + must be a label"))
  elif not bcs.getIndInt(ci.args[1], ofs):
    result.errors.add(ci.err("the second argument to + must be an ind.int"))
  else:
    result.value = A4Value(
      kind: a4vLabel,
      label: ci.args[0].label,
      offset: ci.args[0].offset + ofs
    )

proc btImportBJO(bcs: var A4BlockCompileState, ci: A4BuiltinCallInfo): A4Result =
  if len(ci.args) != 1:
    result.errors.add(ci.err("Expected 1 argument to !importBJO (filename)"))
  elif not ci.args[0].isKind(a4vLabel):
    result.errors.add(ci.err("Expected !importBJO's argument to be a label"))
  elif ci.args[0].offset != 0:
    result.errors.add(ci.err("!importBJO can't make much use of an offset"))
  else:
    try:
      result.value = A4Value(
        kind: a4vObj,
        obj: bcs.compiler.getObject(ci.args[0].label),
        label: ci.args[0].label
      )
    except:
      # Put a dummy in there to prevent this error repeating
      bcs.compiler.objCache[ci.args[0].label] = BBJOF()
      result.errors.add(ci.err("Failed to get object '" & ci.args[0].label & "'"))

proc addConvBuiltins*(compiler: A4Compiler): void =
  compiler.builtins.add("$", A4CompilerBuiltin(
    params: "(source: adjustment, index: ind.int): source",
    help: "Extracts a constant byte out of an address by index (0: lowest)",
    run: btExtract
  ))
  compiler.builtins.add("%", A4CompilerBuiltin(
    params: "(x: adjustment): source",
    help: "Equivalent to $0.",
    run: btZero
  ))
  compiler.builtins.add("^", A4CompilerBuiltin(
    params: "(value: byte, where: label): address",
    help: "Places a label at the use of a byte. This allows further control over self-modification.",
    run: btAttachment
  ))
  compiler.builtins.add("!complex", A4CompilerBuiltin(
    params: "(base: address, ...: byte): address",
    help: "Creates an address from a base and a component byte suffix.",
    run: btComplex
  ))
  compiler.builtins.add("+", A4CompilerBuiltin(
    params: "(base: label, ofs: ind.int): label",
    help: "Adds an offset to a label.",
    run: btAdd
  ))
  compiler.builtins.add("!importBJO", A4CompilerBuiltin(
    params: "(name: label): closure",
    help: "Loads a .bjo file with the given name as a closure.",
    run: btImportBJO
  ))
  # -- These are imported from other files --
  compiler.builtins.add("!call", A4CompilerBuiltin(
    params: "(what: closure, ...): label / void",
    help: "Instances a closure.\nIf put in a situation where it can return a value, an invisible, random argument is prepended and also returned.",
    run: a4biCall
  ))
  compiler.builtins.add("=", A4CompilerBuiltin(
    params: "(target: adjustment, source: adjustment): void",
    help: "Writes instructions to copy the source byte to the target.",
    run: a4biCopy
  ))
