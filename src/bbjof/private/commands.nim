# the various multitool commands

import tables
import sugar
import streams
import strutils
import ./command
import ../../bbjof
import ../merge
import ../verify
import ../read
import ../write
import ../prims
import ../ld

# ---

proc cmdMerge(args: CommandArgs): void =
  var doLink = not args.boolOption("no-link")
  var strip = args.boolOption("strip")
  args.endOptions(-1)

  var obj = BBJOF()
  obj.sizeClass = bbjofsc32

  for fs in args.inputs:
    var xObj = fs.stream.readBBJOF()
    if strip:
      xObj.debugs = @[]
    obj.merge(xObj, fs.name)

  if doLink:
    obj.link()

  args.output.writeBBJOF(obj)

mtCommandRegistry.add("merge", Command(
  help: "[--no-link] [--strip] FILE...",
  run: cmdMerge
))

# ---

proc cmdVerify(args: CommandArgs): void =
  args.endOptions(1)

  for fs in args.inputs:
    var obj = fs.stream.readBBJOF()
    obj.verify()
    if obj.sizeClass == bbjofsc32:
      args.say "32-bit object (BBJO)"
    elif obj.sizeClass == bbjofsc64:
      args.say "64-bit object (BBJX)"
    else:
      args.say "unknown size class (outdated bbjof_verify.nim compared to bbjof.nim)"
    args.say $len(obj.imports) & " imports"
    for imp in obj.imports:
      args.say " " & imp
    args.say $len(obj.exports) & " exports"
    for imp in obj.exports:
      args.say " " & imp.name
    args.say $len(obj.debugs) & " debugs"
    args.say $len(obj.allocs) & " allocs"
    args.say $len(obj.relocs) & " relocs"

mtCommandRegistry.add("verify", Command(
  help: "FILE",
  run: cmdVerify
))

# ---

proc cmdGentbls(args: CommandArgs): void =
  var effectiveOutput = ""
  discard args.stringOption("output", effectiveOutput)
  if effectiveOutput == "":
    effectiveOutput = "."

  var addrBytes: BBJOFInt = 0

  if not args.intOption("address-bytes", addrBytes):
    args.err "Address bytes must be specified."
    quit(QuitFailure)

  var littleEndian = args.boolOption("little-endian")
  var bigEndian = args.boolOption("big-endian")
  if littleEndian == bigEndian:
    args.err "Endianness must be specified."
    quit(QuitFailure)

  args.endOptions(0)

  proc save(file: BBJOF, name: string): void =
    var fs = newFileStream(effectiveOutput & "/" & name & ".bjo", fmWrite)
    fs.writeBBJOF(file)
    fs.close()

  let machine = BBJOFMachineInfo(
    addrBytes: (int) addrBytes,
    littleEndian: littleEndian
  )

  # data
  save(newBBJOFPrim0(machine), "0")
  save(newBBJOFPrimByte(), "byte")
  save(newBBJOFPrimPipe("@1", "@0"), "equ")
  # data (BSS)
  save(newBBJOFPrimBSS(1, 1), "byte_bss")
  save(newBBJOFPrimBSS(256, 256), "page_bss")
  save(newBBJOFPrimBSS(65536, 65536), "bank_bss")
  # code
  save(newBBJOFPrimInstruction(machine, false), "instruction")
  save(newBBJOFPrimInstruction(machine, true), "cbtrampoline")
  save(newBBJOFPrimPipe("@0", "@call"), "goto")
  # various tables
  save(newBBJOFPrimPage((x) => x), "page_id")
  save(newBBJOFPrimPage((x) => x + 1), "page_inc")
  save(newBBJOFPrimPage((x) => x - 1), "page_dec")
  save(newBBJOFPrimPage((x) => (x xor 0xFF) + 1), "page_neg")
  save(newBBJOFPrimPage((x) => x xor 0xFF), "page_not")
  save(newBBJOFPrimBank((x, y) => x + y), "bank_add")
  save(newBBJOFPrimBank((x, y) => x - y), "bank_sub")
  save(newBBJOFPrimBank((x, y) => x and y), "bank_and")
  save(newBBJOFPrimBank((x, y) => x or y), "bank_or")
  save(newBBJOFPrimBank((x, y) => x xor y), "bank_xor")
  # conditional tables
  proc test(v: bool): byte =
    result = (byte) (256 - (addrBytes * 4))
    if v:
      result = (byte) (256 - (addrBytes * 3))
  save(newBBJOFPrimBank((x, y) => test(x == y)), "bank_c_eq")
  save(newBBJOFPrimBank((x, y) => test(x > y)), "bank_c_more")
  save(newBBJOFPrimPage((x) => test(x == 0)), "page_c_zero")
  # bit tables
  for bit in 0 .. 7:
    save(newBBJOFPrimPage((x) => test((bool) (x and (byte) (1 shl bit)))), "page_c_bit" & $bit)
    save(newBBJOFPrimPage((x) => x xor (byte) (1 shl bit)), "page_flip" & $bit)
  # converters
  save(newBBJOFPrimIdFromDecInc(false), "id_from_page_dec")
  save(newBBJOFPrimIdFromDecInc(true), "id_from_page_inc")

mtCommandRegistry.add("gentbls", Command(
  help: "--address-bytes:(1|2|3|4|5|6|7) (--big-endian | --little-endian)",
  run: cmdGentbls,
  overrideOutput: true
))

# ---

proc cmdBinto(args: CommandArgs): void =
  var alignStart: BBJOFInt = 0
  var alignStep: BBJOFInt = 1
  var startSym = ""
  var endSym = ""
  var lenSym = ""
  discard args.intOption("align-start", alignStart)
  discard args.intOption("align", alignStep)
  discard args.stringOption("start-sym", startSym)
  discard args.stringOption("end-sym", startSym)
  discard args.stringOption("len-sym", startSym)
  args.endOptions(1)

  var data = args.inputs[0].stream.readAll()
  let obj = newBBJOFPrimBinary(len(data), alignStart, alignStep, (pos) => (byte) data[pos], startSym, endSym, lenSym)
  # barrier barrier on the wall who is the greatest barrier of them all
  args.output.writeBBJOF(obj)

mtCommandRegistry.add("binto", Command(
  help: "[--align-start:POS] [--align:ALIGN] [--start-sym:SYM] [--end-sym:SYM] [--len-sym:SYM] FILE",
  run: cmdBinto
))

# ---

proc cmdLd(args: CommandArgs): void =
  var memory: BBJOFInt = 0
  if not args.intOption("memory", memory):
    args.err "Memory size must be specified."
    quit(QuitFailure)
  let binary = args.boolOption("binary")
  let force = args.boolOption("force")
  args.endOptions(1)

  var obj = args.inputs[0].stream.readBBJOF()

  obj.alloc((int) memory)
  obj.link()

  if binary:
    let bin = obj.toBinary(force)
    # note: this access mustn't be performed until the binary is made
    # otherwise errors will still allow it to show up
    args.output.write(bin)
  else:
    args.output.writeBBJOF(obj)

mtCommandRegistry.add("ld", Command(
  help: "--memory:MEMORY [--binary [--force]] FILE",
  run: cmdLd
))

# ---

proc cmdDumpRef(obj: BBJOF, r: BBJOFRef): string =
  if r.index < 0:
    var iidx = r.importIdx()
    if iidx >= len(obj.imports):
      result = "INVALID_IMPORT_" & $iidx
    else:
      result = "\"" & obj.imports[iidx] & "\""
  else:
    result = "A" & $r.index
  if r.offset != 0:
    result &= "+" & $r.offset

proc cmdDump(args: CommandArgs): void =
  var debugs = args.boolOption("debugs")
  var map = args.boolOption("map")
  args.endOptions(1)

  var obj = args.inputs[0].stream.readBBJOF()

  if not map:
    args.say "Imports:"
    for k, imp in obj.imports.pairs():
      args.say " " & $k & ": " & imp

    args.say "Exports:"
    for k, exp in obj.exports.pairs():
      args.say " " & $k & ": " & exp.name
      args.say "  = " & obj.cmdDumpRef(exp.target)

    if debugs:
      args.say "Debugs:"
      for k, exp in obj.debugs.pairs():
        args.say " " & $k & ": " & exp.name
        args.say "  = " & obj.cmdDumpRef(exp.target)

    args.say "Allocs:"
    for k, alc in obj.allocs.pairs():
      if alc.size == 0:
        args.say " A" & $k & ": DEGEN @ " & $alc.alignStart
        if alc.alignStep != 0:
          args.say " not yet fixed"
      elif alc.alignStep == 0:
        args.say " A" & $k & ": FIXED @ " & $alc.alignStart
      else:
        args.say " A" & $k & ": FLOATING @ " & $alc.alignStart & " % " & $alc.alignStep
      if alc.data == "":
        args.say "  UNINITIALIZED (" & $alc.size & ")"
      else:
        args.say "  INITIALIZED (" & $len(alc.data) & ")"

    args.say "Relocs:"
    for k, rel in obj.relocs.pairs():
      args.say " " & $k & ": " & obj.cmdDumpRef(rel.target)
      args.say "  = " & obj.cmdDumpRef(rel.source) & "$" & $rel.addrByte
  else:
    var symTab = newOrderedTable[BBJOFInt, string]()
    proc processExp(exp: BBJOFExp): void =
      if exp.target.index < 0:
        return
      let alc = obj.allocs[exp.target.index]
      if alc.alignStep == 0:
        # Using add without replace on purpose, here.
        symTab.add(alc.alignStart + exp.target.offset, exp.name)

    for v in obj.exports:
      processExp(v)

    if debugs:
      for v in obj.debugs:
        processExp(v)

    symTab.sort((a, b) => cmp(a[0], b[0]))
    for k, v in symTab.pairs():
      args.say(toHex(k) & " ? " & v)

mtCommandRegistry.add("dump", Command(
  help: "[--debugs] [--map] FILE",
  run: cmdDump
))
