import ./command
import streams
import tables

proc cmdBMP2BIN(args: CommandArgs): void =
  var aSprites = args.boolOption("sprites")
  var aSpacing = args.boolOption("spacing")
  if aSprites and aSpacing:
    args.err "multiple modes selected"
    quit(QuitFailure)
  args.endOptions(1)

  # now use the options to imply filters
  if aSpacing:
    aSprites = true

  var bmp = args.inputs[0].stream.readAll()
  if len(bmp) != 66614:
    args.err "BMP size must be 66614 bytes - 256x256x8, and 'do not write colour space information' should be set in GIMP."
    quit(QuitFailure)

  # saneify data
  var pixels = ""
  bmp = bmp[1078..len(bmp) - 1]
  var pos = len(bmp) - 0x100
  while pos >= 0:
    pixels &= bmp[pos..pos + 0x100 - 1]
    pos -= 0x100

  # Sprites: Rearrange into 16x16 chunks for sprite use
  if aSprites:
    let oldPixels = pixels
    pixels = ""
    for i in 0..0xFF:
      pos = ((i shr 4) shl 8) or ((i and 0xF) shl 4)
      for j in 0..0xF:
        pixels &= oldPixels[pos..pos + 0xF]
        pos += 0x100

  if aSpacing:
    # Spacing output mode: determine spacing of each sprite
    for i in 0..0xFF:
      pos = i shl 8
      var res = 0
      for j in 0..0xFF:
        if pixels[pos + j] != (char) 0xFF:
          res = (j shr 4) + 1
      args.output.write((uint8) res)
  else:
    # Default output mode: just write pixels as-is
    args.output.write(pixels)

mtCommandRegistry.add("bmp2bin", Command(
  help: "[--sprites] [--spacing] FILE",
  run: cmdBMP2BIN
))
