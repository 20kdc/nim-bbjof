# multitool
import os
import tables
import streams
import parseopt

import ./command

# register commands...
import ./commands

# register a4 command...
import ./a4

# register a4ls command...
import ./a4ls

# register ts command...
import ./ts

# register img commands...
import ./img

# continue

proc help(args: CommandArgs): void =
  args.endOptions(0)
  args.say "--------------------"
  args.say "A4/B BBJOF Multitool"
  args.say "--------------------"
  args.say ""
  args.say "Anything beginning with - (that isn't just '-' itself) is an option."
  args.say "Some options may be passed parameters by postfixing : and the option text."
  args.say "Otherwise, it's an input file."
  args.say "Commands may be passed an output file with '--output:FILE'."
  args.say "Commands which output to a directory treat it as an output directory."
  args.say ""
  args.say "Available commands:"
  for k, v in mtCommandRegistry.pairs():
    args.say " bbjof " & k & " " & v.help

mtCommandRegistry.add("help", Command(run: help))

# Begin parsing

var params = initOptParser()

params.next()
if params.kind != cmdArgument:
  stderr.writeLine "First argument must be a sub-command name (try 'help')"
  quit(QuitFailure)

let userCmd: Command = mtCommandRegistry.getOrDefault(params.key, nil)
if userCmd == nil:
  stderr.writeLine "Unknown command: " & params.key
  quit(QuitFailure)

var userCmdArgs: CommandArgs = CommandArgs(cachedOutput: newFileStream(stdout))

# used to ignore the initial command-name argument
var first = true
for tp, key, val in getopt(params):
  if first:
    first = false
    continue
  if tp == cmdShortOption and key == "":
    # This is actually a stdin argument
    var inp = CommandInput(name: "-")
    if not userCmd.overrideInput:
      inp.stream = newFileStream(stdin)
    userCmdArgs.inputs.add(inp)
  elif tp == cmdLongOption or tp == cmdShortOption:
    userCmdArgs.options.add(key, val)
  elif tp == cmdArgument:
    var inp = CommandInput(name: key)
    if not userCmd.overrideInput:
      inp.stream = openFileStream(inp.name, fmRead)
    userCmdArgs.inputs.add(inp)

# Note: after it's possible that the output stream has been created,
#  it becomes necessary to perform cleanup for that.

if not userCmd.overrideOutput:
  if userCmdArgs.stringOption("output", userCmdArgs.outputPath):
    # Set a future output and remove the cached output filestream (so it'll open the file)
    userCmdArgs.cachedOutput = nil

proc cleanupOutput(): void {.noconv.} =
  if userCmdArgs.cachedOutput != nil:
    userCmdArgs.cachedOutput.close()
    userCmdArgs.cachedOutput = nil

addQuitProc(cleanupOutput)

try:
  userCmd.run(userCmdArgs)
finally:
  cleanupOutput()
