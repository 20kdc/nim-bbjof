import ./command
import ../a4/lex
import ../a4/parse
import ../a4/compile
import ../a4/compile/all
import npeg
import tables
import streams
import json
import strutils

proc cmdA4LS(args: CommandArgs): void =
  var lsDebug: FileStream = nil
  var debugLogLocation: string
  if args.stringOption("debug", debugLogLocation):
    lsDebug = openFileStream(debugLogLocation, fmWrite)
  args.endOptions(0)

  proc debug(text: string): void =
    if lsDebug != nil:
      lsDebug.write(text & "\n")
      lsDebug.flush()

  let lsInput = newFileStream(stdin)
  let lsOutput = newFileStream(stdout)

  proc send(node: JsonNode): void =
    let msgText = node.pretty()
    lsOutput.write("Content-Length: " & $len(msgText) & "\r\n\r\n")
    lsOutput.write(msgText)
    lsOutput.flush()
    debug("sent " & msgText)

  var checker = newA4Checker()
  checker.addAllBuiltins()

  proc reportDiagnostics(uri: string, text: string): void =
    var errors: seq[A4Element]
    var tokens: seq[A4Token]
    try:
      let tokenRes = a4lexer.match(text)
      if not tokenRes.ok:
        errors.add(A4Element(
          kind: a4eError,
          position: a4ConvertPosition(text, "", tokenRes.matchMax),
          name: "Tokenization error"
        ))
      else:
        tokens = a4ConvertTokens(text, "", tokenRes)
    except NPegException as e:
      errors.add(A4Element(
        kind: a4eError,
        position: a4ConvertPosition(text, "", e.matchMax),
        name: e.msg
      ))

    var resParse = a4parseBlock(tokens)
    try:
      errors &= checker.compile("file", resParse).errors
    except CatchableError as e:
      errors.add(A4Element(
        kind: a4eError,
        name: "internal uncaught compiler error: " & e.msg
      ))

    var diagnostics: seq[JsonNode]

    for v in errors:
      diagnostics.add(%* {
        "range": {
          "start": {
            "line": v.position.line,
            "character": v.position.character
          },
          "end": {
            "line": v.position.line,
            "character": v.position.character
          }
        },
        "message": v.name
      })
    send(%* {
      "jsonrpc": "2.0",
      "method": "textDocument/publishDiagnostics",
      "params": {
        "uri": uri,
        "diagnostics": diagnostics
      }
    })

  debug("-- start --")

  while true:
    try:
      var contentLength = 0
      while true:
        let line = lsInput.readLine()
        if line == "":
          break
        let parts = line.split(":")
        if parts[0].strip().toLower() == "content-length":
          contentLength = parseInt(parts[1].strip())
      let msgText = lsInput.readStr(contentLength)
      debug("request " & msgText)
      let msg = parseJson(msgText)
      let req = msg{"method"}
      if req != nil:
        if req.getStr() == "initialize":
          send(%* {
            "jsonrpc": "2.0",
            "id": msg["id"],
            "result": {
              "capabilities": {
                # what this means is, "send the whole file"
                "textDocumentSync": 1,
                "completionOptions": {
                  # ! is the builtin thingy
                  "triggerCharacters": ["!"]
                }
              }
            }
          })
        elif req.getStr() == "textDocument/didOpen":
          # apparently this got shuffled between the 1.0 spec and later versions
          reportDiagnostics(msg["params"]["textDocument"]["uri"].getStr(), msg["params"]["textDocument"]["text"].getStr())
        elif req.getStr() == "textDocument/didChange":
          # same here, notice the change in placement of uri
          reportDiagnostics(msg["params"]["textDocument"]["uri"].getStr(), msg["params"]["contentChanges"][0]["text"].getStr())
        elif req.getStr() == "textDocument/completion":
          var results = newJArray()
          for k, v in checker.builtins.pairs():
            results.add(%* {
              "label": k,
              "documentation": v.params & "\n" & v.help
            })
          send(%* {
            "jsonrpc": "2.0",
            "id": msg["id"],
            "result": results
          })
    except IOError:
      break
  debug("-- end --")

mtCommandRegistry.add("a4ls", Command(
  help: "[--debug:LOGFILE]",
  run: cmdA4LS,
  overrideInput: true,
  overrideOutput: true
))
