# multitool command registry
import sugar
import tables
import streams
import strutils
import ../parseint
import ../../bbjof

type
  CommandInput* = object
    name*: string
    stream*: FileStream
  CommandArgs* = ref object
    outputPath*: string
    cachedOutput*: FileStream
    options*: Table[string, string]
    inputs*: seq[CommandInput]
  Command* = ref object
    help*: string
    run*: (args: CommandArgs) -> void
    overrideOutput*: bool
    overrideInput*: bool

proc output*(ca: CommandArgs): FileStream =
  if ca.cachedOutput == nil:
    ca.cachedOutput = openFileStream(ca.outputPath, fmWrite)
  result = ca.cachedOutput

proc say*(ca: CommandArgs, text: string): void =
  ca.output.writeLine(text)

proc err*(ca: CommandArgs, text: string): void =
  stderr.writeLine(text)

proc intOption*(ca: CommandArgs, text: string, val: var BBJOFInt): bool =
  ## Finds and removes an integer option, placing the value in 'val'.
  result = false
  if ca.options.contains(text):
    val = parseint.parseBBJOFInt(ca.options[text])
    ca.options.del(text)
    result = true

proc boolOption*(ca: CommandArgs, text: string): bool =
  ## Finds and removes a boolean option.
  result = false
  if ca.options.contains(text):
    if ca.options[text] != "":
      ca.err "Warning: Option " & text & " shouldn't have a parameter"
    ca.options.del(text)
    result = true

proc stringOption*(ca: CommandArgs, text: string, val: var string): bool =
  ## Finds and removes a string option, placing the value in 'val'.
  result = false
  if ca.options.contains(text):
    val = ca.options[text]
    ca.options.del(text)
    result = true

proc endOptions*(ca: CommandArgs, inputs: int): void =
  ## Ends the recognized options (implying remaining options are unrecognized) and verifies the amount of inputs (-1 for unlimited).
  var failed = false
  for v in ca.options.keys():
    ca.err "Unknown option '" & v & "'"
    failed = true
  if inputs != -1:
    if len(ca.inputs) != inputs:
      ca.err "Expects exactly " & $inputs & " inputs, got " & $len(ca.inputs)
      failed = true
  if failed:
    quit(QuitFailure)

var mtCommandRegistry* = newTable[string, Command]()
