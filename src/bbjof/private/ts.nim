import ./command
import streams
import tables
import ../../bbjof

proc cmdTS(args: CommandArgs): void =
  var le = args.boolOption("little-endian")
  var be = args.boolOption("big-endian")
  var debug = args.boolOption("debug")
  if le == be:
    args.err "Must specify one of --little-endian or --big-endian"
    quit(QuitFailure)
  var addressBytes: BBJOFInt = 0
  if not args.intOption("address-bytes", addressBytes):
    args.err "Must specify --address-bytes"
    quit(QuitFailure)
  args.endOptions(1)

  var memoryInit = args.inputs[0].stream.readAll()
  var memorySize = 1 shl (8 * addressBytes)
  if len(memoryInit) > memorySize:
    args.err "Too much data in memory init file"
    quit(QuitFailure)
  var memory = newString(memorySize)
  for i in 0 .. len(memoryInit) - 1:
    memory[i] = memoryInit[i]

  let instructionSize: BBJOFInt = addressBytes * 3

  proc read(p: BBJOFInt): uint8 =
    if p < 0 or p >= memorySize:
      result = 0
    elif p == memorySize - 1:
      result = (uint8) stdin.readChar()
    else:
      result = (uint8) memory[(int) p]

  proc readAddress(base: BBJOFInt): int =
    var p: BBJOFInt = base
    if le:
      p += addressBytes - 1
    for i in 0 .. addressBytes - 1:
      result = (result shl 8) or (int) read(p)
      if be:
        p = p + 1
      else:
        p = p - 1

  proc write(p: BBJOFInt, v: uint8): void =
    if p == memorySize - 1:
      var c: array[0..0, uint8]
      c[0] = (uint8) v
      discard stdout.writeBytes(c, 0, 1)
      stdout.flushFile()
    elif p >= 0 and p < memorySize:
      memory[p] = (char) v

  # Run the VM
  var ip: BBJOFInt = 0
  while true:
    let originalIP = ip
    if ip > memorySize - instructionSize:
      break
    let src = readAddress(ip)
    ip += addressBytes
    let dst = readAddress(ip)
    ip += addressBytes
    if debug:
      args.err "@" & $originalIP & ": " & $src & " -> " & $dst
    let val = read(src)
    if debug:
      args.err " " & $val
    write(dst, val)
    # note that this read must occur after the copy
    ip = readAddress(ip)

mtCommandRegistry.add("ts", Command(
  help: "--address-bytes:(1|2|3) (--big-endian | --little-endian) [--debug] FILE",
  run: cmdTS
))
