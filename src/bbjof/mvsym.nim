import ../bbjof
import tables
import algorithm

type
  BBJOFMvsymMode* = enum
    # Don't nuke anything
    bbjofmvmNormal,
    # Nuke all symbols beginning with "@" that aren't otherwise handled, and this means imports too
    bbjofmvmLocal,
    # Nuke all unhandled exports
    bbjofmvmCleanupExports
  BBJOFMvsymTarget* = object
    ## This is an alias target.
    # This is the new name (or new prefix, if structures are involved)
    name*: string
    # This is the offset "added to the target".
    # So if an import is being moved, that import gains this much offset.
    # If an export is being moved, that export *loses* this much offset.
    # This logic is important to ensure it balances out for imports and exports in the original file,
    #  and it also makes sense that if you're trying to turn "a" into "b+5" then "b" should be thus "a-5".
    # If any structures are involved in the move, an error occurs unless this is 0.
    offset*: BBJOFInt

proc bbjofDeleteSortedIndices*[T](target: var seq[T], indices: seq[int]): void =
  ## Removes various elements by-index.
  ## The indices must be sorted.
  # This version doesn't use .delete because .delete is evil and out to destroy performance
  var tmp: seq[T]
  var qi: int = 0
  for k, v in target.pairs():
    if qi < len(indices):
      if indices[qi] == k:
        qi += 1
        continue
    tmp.add(v)
  target = tmp

proc mvsym*(obj: BBJOF, tbl: TableRef[string, BBJOFMvsymTarget], exclusive: BBJOFMvsymMode): void =
  ## Aliases symbols.
  ## If exclusive is true, deletes all symbols which aren't aliased.
  # Keys in longest-to-shortest order (which solves structural prioritization)
  var syms: seq[string]
  for v in tbl.keys():
    syms.add(v)
  # Keep in mind, the idea here is to sort by inverse length.
  syms.sort do (a, b: string) -> int:
    cmp(len(b), len(a))

  # Remaps-in-place something.
  # Note that 'mul' is also used as an exports flag. It's just simpler that way.
  proc remap(name: var string, offset: var BBJOFInt, mul: BBJOFInt): void =
    for v in syms:
      let val = tbl[v]
      let rel = name.bbjofSymRelation(v)
      if rel == bbjofsrDirect:
        name = val.name
        offset += val.offset * mul
        return
      elif rel == bbjofsrIndirect:
        if val.name == "":
          name = ""
        else:
          name = val.name & name[len(v)..len(name) - 1]
        offset += val.offset * mul
        return
    if exclusive == bbjofmvmCleanupExports and mul == -1:
      name = ""
    elif exclusive == bbjofmvmLocal and name[0] == '@':
      name = ""

  var importOffsets: seq[BBJOFInt]

  for k, v in obj.imports.mpairs():
    var ofs: BBJOFInt = 0
    let original: string = v
    remap(v, ofs, 1)
    importOffsets.add(ofs)
    if v == "":
      raise newException(IOError, "Attempted to delete import '" & original & "'")

  for r in obj.refs(true):
    if r.index < 0:
      r.offset += importOffsets[r.importIdx()]

  # Luckily, if imports need removing, it's an instant error.
  # And exports are easy to remove.

  var exportsToRemove: seq[int]
  for k, v in obj.exports.mpairs():
    remap(v.name, v.target.offset, -1)
    if v.name == "":
      exportsToRemove.add(k)

  obj.exports.bbjofDeleteSortedIndices(exportsToRemove)
