# A4 / ByteByteJump Object Format
#  structural definition and basic utilities

import strutils

type

  # the universal integer type between size classes
  BBJOFInt* = int64

  # the order of values in this enum defines the increasing precedence
  BBJOFSizeClasses* = enum
    bbjofsc32, bbjofsc64

  # various internal structs
  BBJOFRef* = object
    index*: BBJOFInt
    offset*: BBJOFInt
  BBJOFExp* = object
    target*: BBJOFRef
    name*: string
  BBJOFAlloc* = object
    # The size of the allocation.
    # Note: The "negative size flag" is replaced with data being empty/not empty.
    size*: BBJOFInt
    # A theoretical valid place for the allocation.
    alignStart*: BBJOFInt
    # The step between valid places for the allocation. 0 means unmovable.
    alignStep*: BBJOFInt
    # This must be empty for uninitialized allocs. Otherwise it must match size.
    data*: string
  BBJOFRel* = object
    # The source reference ("What to write").
    source*: BBJOFRef
    # The specific address byte (where 0 is the lowest byte to avoid needing to know address size)
    addrByte*: BBJOFInt
    # The target reference ("Where to write it"). Can't be an import index.
    target*: BBJOFRef

  BBJOF* = ref object
    ## A BBJOF file represents a set of data that may be parameterized and embedded into a ByteByteJump program.
    sizeClass*: BBJOFSizeClasses
    imports*: seq[string]
    exports*: seq[BBJOFExp]
    debugs*: seq[BBJOFExp]
    allocs*: seq[BBJOFAlloc]
    relocs*: seq[BBJOFRel]

# assorted utils

proc bbjofSizeMax*(a: BBJOFSizeClasses, b: BBJOFSizeClasses): BBJOFSizeClasses {.inline.} =
  result = a
  if a < b:
    result = b

func importIdx*(a: BBJOFRef): BBJOFInt {.inline.} =
  (-a.index) - 1

proc setImportIdx*(a: var BBJOFRef, v: BBJOFInt): void {.inline.} =
  a.index = -(v + 1)

iterator refs*(target: BBJOF, debugs: bool): var BBJOFRef =
  ## Iterates over all BBJOFRefs in a way that allows mutating them.
  for v in target.exports.mitems():
    yield v.target
  if debugs:
    for v in target.debugs.mitems():
      yield v.target
  for v in target.relocs.mitems():
    yield v.source
    yield v.target

func findExport*(target: BBJOF, name: string): int =
  ## Finds an export by name, returns index or -1.
  ## This is a linear search all by itself, DON'T overdo this.
  ## In practice this is supposed to be used in the A4 compiler to
  ##  lookup constants that are needed immediately.
  for k, v in target.exports.pairs():
    if v.name == name:
      return k
  return -1

func hasExport*(target: BBJOF, name: string): bool =
  ## Checks if an object has an export.
  ## Use infrequently, this is a linear search all by itself.
  return target.findExport(name) != -1

# symbol relations

type
  BBJOFSymRelation* = enum
    bbjofsrNone,
    bbjofsrDirect,
    bbjofsrIndirect

func bbjofSymRelation*(path: string, parent: string): BBJOFSymRelation =
  ## Returns the relation between two symbols.
  if path == parent:
    bbjofsrDirect
  elif path.startsWith(parent & "."):
    bbjofsrIndirect
  else:
    bbjofsrNone

func bbjofGetClosureInstance*(child: string): tuple[ok: bool, name: string] =
  ## Returns (true, containingClosureInstance) or (false, "").
  ## The containing closure instance value has a "." after it to handle the "root" case.
  ## This assumes you have already stripped out the start component (perhaps with bbjofSymNext).
  var here = 0
  while here < len(child):
    if child[here] == '@':
      # Containing closure instance is...
      if here == 0:
        # ...where we already were!
        return (true, "")
      elif here == 1:
        # This shouldn't really be valid (it implies ".."), but handle it as correctly as possible
        return (true, ".")
      else:
        # ...In some sub-component!
        return (true, child[0..here - 1])
    var where = child.find('.', here)
    if where == -1:
      # Out of components...
      break
    here = where + 1
  # Oh well.
  return (false, "")

when isMainModule:
  import bbjof/private/multitool
