import BytePusherVM from "./vm.js";
import BytePusherRenderer from "./render.js";
import BytePusherKeyboard from "./kb.js";
import BytePusherAudio from "./audio.js";

export default function main() {

  var stat0 = document.getElementById("stat0");
  var stat1 = document.getElementById("stat1");
  var stat2 = document.getElementById("stat2");
  var stat3 = document.getElementById("stat3");
  var stat4 = document.getElementById("stat4");

  var canvas = document.getElementById("bytepusher_surface");

  var vm = new BytePusherVM();
  var renderer = new BytePusherRenderer(canvas);
  var keyboard = new BytePusherKeyboard(document.body);
  let audio = new BytePusherAudio();

  var frame = 0;

  // File loader 1

  var datareq = new XMLHttpRequest();
  datareq.open("GET", "in.bp");
  datareq.responseType = "arraybuffer";
  datareq.onload = () => {
    vm.load(new Uint8Array(datareq.response));
    frame = 0;
  };
  datareq.send()

  // File loader 2

  var bpfile = document.getElementById("bpfile");
  bpfile.onchange = () => {
    audio.startup();
    bpfile.files[0].arrayBuffer().then((ab) => {
      vm.load(new Uint8Array(ab));
      frame = 0;
    });
  };

  // Other stuff

  var lastFrame = undefined;
  var builtDeltaTime = 0;
  var frameLength = 1000 / 60;

  let halted = false;
  let lastCycles = 0;

  function runFrames(frameTime) {
    var frames = 1;
    if (lastFrame != undefined) {
      var deltaTime = frameTime - lastFrame;
      builtDeltaTime += deltaTime;
      frames = 0;
      // why > 0? simple: try to get things done "early"
      while (builtDeltaTime > 0) {
        builtDeltaTime -= frameLength;
        frames++;
      }
    }
    lastFrame = frameTime;

    // schedule again before even starting
    window.requestAnimationFrame(runFrames);

    if ((frames == 0) || halted)
      return;
    let audioValid = true;
    if (frames > 10) {
      // the browser probably backgrounded us
      frames = 1;
      audioValid = false;
    }
    for (var i = 0; i < frames; i++) {
      // this is the sole purpose of audio.js :)
      keyboard.frame(vm.memory);
      lastCycles = vm.frame();
      if (audioValid)
        audio.push(vm.memory, (vm.memory[6] << 16) | (vm.memory[7] << 8));
      frame++;
    }
    const a = vm.memory[0];
    const b = vm.memory[1];
    const c = vm.memory[2];
    const d = vm.memory[3];
    const e = vm.memory[4];
    stat0.innerHTML = ((c << 16) | (d << 8) | e).toString(16).padStart(6, "0");
    stat1.innerHTML = Math.floor((lastCycles / 65537) * 100).toString().padStart(2, "0");
    stat2.innerHTML = lastCycles.toString(16).padStart(4, "0");
    stat3.innerHTML = (frame & 0xFFFF).toString(16).padStart(4, "0");
    stat4.innerHTML = audio.audioGap.toString(16).padStart(4, "0");
    stat5.innerHTML = ((a << 8) | b).toString(16).padStart(4, "0");
    renderer.push(vm.memory, vm.memory[5] << 16);
  }

  window.requestAnimationFrame(runFrames);

  return {
    halt: function () {
      halted = !halted;
      if (halted) {
        audio.shutdown();
      } else {
        audio.startup();
      }
    }
  };
}
