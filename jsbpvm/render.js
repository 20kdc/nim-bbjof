const palette = [];
for (let c = 0; c < 256; c++) {
  let ec = c;
  if (ec > 215)
    ec = 0;
  let r = Math.floor(ec / 36);
  let g = Math.floor(ec / 6) % 6;
  let b = ec % 6;
  palette[c] = [0x33 * r, 0x33 * g, 0x33 * b];
}

export default class BytePusherRenderer {
  constructor(canvas) {
    this.context = canvas.getContext("2d");
    this.renderSurface = this.context.createImageData(256, 256);
    for (let i = 0; i < 256 * 256 * 4; i++)
      this.renderSurface.data[i] = 255;
  }
  push(memory, rootAddr) {
    for (let i = 0; i < 256 * 256; i++) {
      var a = i << 2;
      var c = palette[memory[rootAddr | i]];
      this.renderSurface.data[a] = c[0];
      this.renderSurface.data[a + 1] = c[1];
      this.renderSurface.data[a + 2] = c[2];
    }
    this.context.putImageData(this.renderSurface, 0, 0);
  }
}

