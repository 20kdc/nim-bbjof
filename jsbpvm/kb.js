var keyboardA = 0;
var keyboardB = 0;

var keys = {};
keys["1"] = 0x1; keys["2"] = 0x2; keys["3"] = 0x3; keys["4"] = 0xC;
keys["q"] = 0x4; keys["w"] = 0x5; keys["e"] = 0x6; keys["r"] = 0xD;
keys["a"] = 0x7; keys["s"] = 0x8; keys["d"] = 0x9; keys["f"] = 0xE;
keys["z"] = 0xA; keys["x"] = 0x0; keys["c"] = 0xB; keys["v"] = 0xF;

/**
 * BytePusherKeyboard - keyboard, uses PuteBysh's layout.
 * Attaches itself to the target element automatically.
 * Also ensures that a key that gets received is definitely hit on the VM side.
 */
export default class BytePusherKeyboard {
  constructor(element) {
    this.aPrimary = 0;
    this.bPrimary = 0;
    this.aImmediate = 0;
    this.bImmediate = 0;
    this.elements = {};
    this.elementsBCN = {};

    for (const k in keys) {
      let btn = document.getElementById("kp" + k);
      this.elements[keys[k]] = btn;
      this.elementsBCN[keys[k]] = btn.className;
      btn.onpointerdown = (ev) => {
        this.pressKey(keys[k]);
        btn.setPointerCapture(ev.pointerId);
        ev.preventDefault();
      };
      btn.onpointerup = (ev) => {
        this.releaseKey(keys[k]);
        btn.releasePointerCapture(ev.pointerId);
        ev.preventDefault();
      };
    }

    element.onkeydown = (ev) => {
      if (keys[ev.key] != undefined) {
        this.pressKey(keys[ev.key]);
      }
    };
    element.onkeyup = (ev) => {
      if (keys[ev.key] != undefined) {
        this.releaseKey(keys[ev.key]);
      }
    };
  }

  pressKey(key) {
    this.elements[key].className = this.elementsBCN[key] + " active";
    if (key > 7) {
      const val = 1 << (key - 8);
      this.aImmediate |= val;
      this.aPrimary |= val;
    } else {
      const val = 1 << key;
      this.bImmediate |= val;
      this.bPrimary |= val;
    }
  }
  releaseKey(key) {
    this.elements[key].className = this.elementsBCN[key];
    if (key > 7) {
      this.aPrimary &= ~(1 << (key - 8));
    } else {
      this.bPrimary &= ~(1 << key);
    }
  }

  frame(memory) {
    memory[0] = this.aImmediate | this.aPrimary;
    memory[1] = this.bImmediate | this.bPrimary;
    this.aImmediate = 0;
    this.bImmediate = 0;
  }
}

