/**
 * The asm.js VM core.
 * This should not be considered API.
 * Hell, this shouldn't even be exposed...
 */
function bytepusherVMAsmjsCore(stdlib, foreign, heap) {
  "use asm";

  var memory = new stdlib.Uint8Array(heap);

  function getAddress(addr) {
    addr = addr | 0;

    var a = 0;
    var b = 0;
    var c = 0;

    if ((addr|0) < 0x1000000)
      a = ((memory[addr] | 0) << 16) | 0;

    if ((addr|0) < 0xFFFFFF)
      b = ((memory[(addr + 1) | 0] | 0) << 8) | 0;

    if ((addr|0) < 0xFFFFFE)
      c = memory[(addr + 2) | 0] | 0;

    return (a | b | c) | 0;
  }

  function frame() {
    var ip = 0;
    var previousIP = 0;
    var cycle = 0;
    var a = 0;
    var b = 0;

    ip = getAddress(2) | 0;

    while ((cycle | 0) < 0x10000) {
      a = getAddress(ip) | 0;
      b = getAddress((ip + 3) | 0) | 0;
      memory[b] = memory[a] | 0;
      previousIP = ip;
      ip = getAddress((ip + 6) | 0) | 0;
      cycle = (cycle + 1) | 0;

      // Optimization: Exit early if the copy was a NOP and the jump address is here
      if ((a|0) == (b|0))
        if ((previousIP|0) == (ip|0))
          break;
    }
    return cycle | 0;
  }

  return {
    frame: frame
  };
}

/**
 * A wrapping class that provides useful methods for controlling the asm.js VM core.
 */
export default class BytePusherVM {
  constructor() {
    this.memoryArrayBuffer = new ArrayBuffer(0x1000000);
    this.memory = new Uint8Array(this.memoryArrayBuffer);
    this._core = bytepusherVMAsmjsCore(window, null, this.memoryArrayBuffer);
  }

  /**
   * Loads a given BytePusher image (as Uint8Array) into memory.
   */
  load(image) {
    for (let i = 0; i < 0x1000000; i++)
      this.memory[i] = i < image.length ? image[i] : 0;
  }

  /**
   * Gets an address from the core memory.
   */
  getAddress(addr) {
    return this._core.getAddress(addr);
  }

  /**
   * Performs a frame.
   * Returns the amount of cycles actually performed.
   */
  frame() {
    return this._core.frame();
  }
}

