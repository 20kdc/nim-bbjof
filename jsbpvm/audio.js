/**
 * Handles the audio side of things in a way that currently works.
 * Most variables aren't API except audioGap for debug reasons.
 * Heavily commented because otherwise I can't understand it.
 * I wrote this a while back and had to modify it to stop it eating all RAM
 *  and occasionally glitching (GC lag probably)
 */
export default class BytePusherAudio {

  constructor() {
    // init to off
    this._audioContext = null;
    this._audioProcessor = null;
    this._audioBuf = null;
    this._audioNode = null;

    // This is the main internal ring buffer.
    this._audioBuffer = new Uint8Array(0x1000);
    // This is the write pointer for the main internal ring buffer.
    this._audioWP = 0;
    // This is the read pointer for the main internal ring buffer.
    this._audioRP = 0;

    // This is the audio gap (amount of samples left).
    // Note that modifying this is not allowed.
    // It's kept in sync manually with _audioWP and _audioRP,
    //  mess with that and stuff breaks.
    this.audioGap = 0;

    this.lastRateModifier = 0;
  }

  shutdown() {
    if (this._audioNode != null) {
      this._audioNode.disconnect(this._audioProcessor);
      this._audioProcessor.disconnect(this._audioContext.destination);
      this._audioContext = null;
      this._audioProcessor = null;
      this._audioBuf = null;
      this._audioNode = null;
    }
  }

  startup() {
    this.shutdown();

    const audioProcessorBufferLength = 1024;

    this._audioContext = new AudioContext();
    this._audioProcessor = this._audioContext.createScriptProcessor(audioProcessorBufferLength, 1, 1);

    // This is used to get the audio system to do something.
    this._audioBuf = this._audioContext.createBuffer(1, 256 * 10, 22050);
    this._audioNode = this._audioContext.createBufferSource();

    this._audioProcessor.onaudioprocess = (ev) => {
      // This is the rate measured in BytePusher samples per audioContext sample.
      // Thus it is ideally 15360 / 44100.
      const rateIdeal = 15360.0 / this._audioContext.sampleRate;

      // This is then modified so that when the gap is at half the ring buffer length
      //  things go at standard speed, but as the VM speed changes,
      //  we change speed with it.
      let rateModifier = this.audioGap / (this._audioBuffer.length / 2);

      rateModifier = (Math.sign(rateModifier - 1) * Math.pow(rateModifier - 1, 2)) + 1;

      rateModifier = (rateModifier + (this.lastRateModifier * 7)) / 8;

      this.lastRateModifier = rateModifier;

      // This is the actual input-samples-per-output-sample rate.
      const rate = rateIdeal * rateModifier;

      const audioFloats = ev.outputBuffer.getChannelData(0);
      const length = audioProcessorBufferLength;

      // How far are we through the current sample?
      let accum = 0;

      // Essentially "floored accumulator".
      // Updated after each sample - used to count actual eaten samples
      //  as opposed to the amount that we should've eaten.
      let accumInt = 0;

      // The current sample.
      let val = this._audioPullVal();

      for (var i = 0; i < length; i++) {
        audioFloats[i] = val;
        accum += rate;
        if (accum > accumInt) {
          accumInt++;
          val = this._audioPullVal();
        }
      }
    };

    this._audioNode.buffer = this._audioBuf;
    this._audioNode.loop = true;
    this._audioNode.connect(this._audioProcessor);
    this._audioProcessor.connect(this._audioContext.destination);
    this._audioNode.start();
  }

  _audioPullVal() {
    if (this.audioGap != 0) {
      // Pull value...
      let val = (this._audioBuffer[this._audioRP] ^ 0x80) / 255.0;
      // Normalize to -1 to 1
      val = (val * 2) - 1;
      // Apparently it's too loud or something
      val /= 2;
      // Update tracking
      this._audioRP++;
      this._audioRP %= this._audioBuffer.length;
      this.audioGap--;
      return val;
    }
    return 0;
  }

  push(buffer, start) {
    for (let i = 0; i < 256; i++) {
      if (this.audioGap == this._audioBuffer.length - 1)
        break;
      // Push value...
      this._audioBuffer[this._audioWP] = buffer[start + i];
      // Update tracking
      this._audioWP++;
      this._audioWP %= this._audioBuffer.length;
      this.audioGap++;
    }
  }

}
